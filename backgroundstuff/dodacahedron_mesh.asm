;Pentakis Dodecahedron

C0 = 0.927050983124842272306880251548; = 3 * (sqrt(5) - 1) / 4
C1 = 1.33058699733550141141687582919;  = 9 * (9 + sqrt(5)) / 76
C2 = 2.15293498667750705708437914596;  = 9 * (7 + 5 * sqrt(5)) / 76
C3 = 2.427050983124842272306880251548; = 3 * (1 + sqrt(5)) / 4
fact = 20

vertx3 
   !byte 0.0 * fact 
   !byte 0.0 * fact 
   !byte 0.0 * fact 
   !byte 0.0 * fact 
   !byte C3 * fact 
   !byte C3 * fact 
   !byte -C3 * fact 
   !byte -C3 * fact 
   !byte C0 * fact 
   !byte C0 * fact 
   !byte -C0 * fact 
   !byte -C0 * fact 
   !byte C1 * fact 
   !byte C1 * fact 
   !byte -C1 * fact 
   !byte -C1 * fact 
   !byte C2 * fact 
   !byte C2 * fact 
   !byte -C2 * fact 
   !byte -C2 * fact 
   !byte 0.0 * fact 
   !byte 0.0 * fact 
   !byte 0.0 * fact 
   !byte 0.0 * fact 
   !byte 1.5 * fact 
   !byte 1.5 * fact 
   !byte 1.5 * fact 
   !byte 1.5 * fact 
   !byte 1.5 * fact 
   !byte 1.5 * fact 
   !byte 1.5 * fact 
   !byte 1.5 * fact 

verty3 
   !byte C0 * fact 
   !byte C0 * fact 
   !byte -C0 * fact 
   !byte -C0 * fact 
   !byte 0.0 * fact 
   !byte 0.0 * fact 
   !byte 0.0 * fact 
   !byte 0.0 * fact 
   !byte C3 * fact 
   !byte -C3 * fact 
   !byte C3 * fact 
   !byte -C3 * fact 
   !byte 0.0 * fact 
   !byte 0.0 * fact 
   !byte 0.0 * fact 
   !byte 0.0 * fact 
   !byte C1 * fact 
   !byte -C1 * fact 
   !byte C1 * fact 
   !byte -C1 * fact 
   !byte C2 * fact 
   !byte C2 * fact 
   !byte -C2 * fact 
   !byte -C2 * fact 
   !byte 1.5 * fact 
   !byte 1.5 * fact 
   !byte 1.5 * fact 
   !byte 1.5 * fact 
   !byte 1.5 * fact 
   !byte 1.5 * fact 
   !byte 1.5 * fact 
   !byte 1.5 * fact 

vertz3 
   !byte C3 * fact 
   !byte -C3 * fact 
   !byte C3 * fact 
   !byte -C3 * fact 
   !byte C0 * fact 
   !byte -C0 * fact 
   !byte C0 * fact 
   !byte -C0 * fact 
   !byte 0.0 * fact 
   !byte 0.0 * fact 
   !byte 0.0 * fact 
   !byte 0.0 * fact 
   !byte C2 * fact 
   !byte -C2 * fact 
   !byte C2 * fact 
   !byte -C2 * fact 
   !byte 0.0 * fact 
   !byte 0.0 * fact 
   !byte 0.0 * fact 
   !byte 0.0 * fact 
   !byte C1 * fact 
   !byte -C1 * fact 
   !byte C1 * fact 
   !byte -C1 * fact 
   !byte 1.5 * fact 
   !byte 1.5 * fact 
   !byte 1.5 * fact 
   !byte 1.5 * fact 
   !byte 1.5 * fact 
   !byte 1.5 * fact 
   !byte 1.5 * fact 
   !byte 1.5 * fact 




;faces
  !byte 12, 0, 2, 12, 1
  !byte 12, 2, 26, 12, 1
  !byte 12, 26, 4, 12, 1
  !byte 12, 4, 24, 12, 1
  !byte 12, 24, 0, 12, 1
  !byte 13, 3, 1, 13, 1
  !byte 13, 1, 25, 13, 1
  !byte 13, 25, 5, 13, 1
  !byte 13, 5, 27, 13, 1
  !byte 13, 27, 3, 13, 1
  !byte 14, 2, 0, 14, 1
  !byte 14, 0, 28, 14, 1
  !byte 14, 28, 6, 14, 1
  !byte 14, 6, 30, 14, 1
  !byte 14, 30, 2, 14, 1
  !byte 15, 1, 3, 15, 1
  !byte 15, 3, 31, 15, 1
  !byte 15, 31, 7, 15, 1
  !byte 15, 7, 29, 15, 1
  !byte 15, 29, 1, 15, 1
  !byte 16, 4, 5, 16, 1
  !byte 16, 5, 25, 16, 1
  !byte 16, 25, 8, 16, 1
  !byte 16, 8, 24, 16, 1
  !byte 16, 24, 4, 16, 1
  !byte 17, 5, 4, 17, 1
  !byte 17, 4, 26, 17, 1
  !byte 17, 26, 9, 17, 1
  !byte 17, 9, 27, 17, 1
  !byte 17, 27, 5, 17, 1
  !byte 18, 7, 6, 18, 1
  !byte 18, 6, 28, 18, 1
  !byte 18, 28, 10, 18, 1
  !byte 18, 10, 29, 18, 1
  !byte 18, 29, 7, 18, 1
  !byte 19, 6, 7, 19, 1
  !byte 19, 7, 31, 19, 1
  !byte 19, 31, 11, 19, 1
  !byte 19, 11, 30, 19, 1
  !byte 19, 30, 6, 19, 1
  !byte 20, 8, 10, 20, 1
  !byte 20, 10, 28, 20, 1
  !byte 20, 28, 0, 20, 1
  !byte 20, 0, 24, 20, 1
  !byte 20, 24, 8, 20, 1
  !byte 21, 10, 8, 21, 1
  !byte 21, 8, 25, 21, 1
  !byte 21, 25, 1, 21, 1
  !byte 21, 1, 29, 21, 1
  !byte 21, 29, 10, 21, 1
  !byte 22, 11, 9, 22, 1
  !byte 22, 9, 26, 22, 1
  !byte 22, 26, 2, 22, 1
  !byte 22, 2, 30, 22, 1
  !byte 22, 30, 11, 22, 1
  !byte 23, 9, 11, 23, 1
  !byte 23, 11, 31, 23, 1
  !byte 23, 31, 3, 23, 1
  !byte 23, 3, 27, 23, 1
  !byte 23, 27, 9, 23, 1
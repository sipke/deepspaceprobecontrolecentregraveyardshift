
//*******************************
//                              *
// stephen judd                 *
// george taylor                *
// started: 7/11/94             *
// finished: 7/19/94            *
// v2.0 completed: 12/17/94     *
// v3.0 completed: 3/20/95      *
// v3.1 completed: 6/14/95      *
// v3.2 completed: 6/15/95      *
//                              *
// well, if all goes well this  *
// program will rotate a cube.  *
//                              *
// v2.0 + new and improved!     *
// now with faster routines,    *
// hidden surfaces, filled      *
// faces, and extra top secret  *
// text messages!               *
//                              *
// v3.0 + fast chunky line      *
// routine.                     *
//                              *
// v3.1 + general polygon plot  *
// with hidden faces (x-product)*
// and zoom feature.            *
//                              *
// v3.2 + eor-buffer filling    *
//                              *
// this program is intended to  *
// accompany the article in     *
// c=hacking, jun. 95 issue.    *
// for details on this program, *
// read the article!            *
//                              *
// write to us!                 *
//                              *
// myself when young did        *
// eagerly frequent             *
// doctor and saint, and heard  *
// great argument               *
//  about it and about: but     *
//  evermore                    *
// came out by the same door    *
// as in i went.                *
//    - rubaiyat                *
//                              *
// though i speak with the      *
// tongues of men and of angles *
// and have not love, i am      *
// become as sounding brass, or *
// a tinkling cymbal.           *
//    - 1 corinthians 13        *
//                              *
// p.s. this was written using  *
//      merlin 128.             *
//*******************************

 * = $8000

// constants

.var buff1 = $3000 //first character set
.var buff2 = $3800 //second character set
.var eorbuf = $4000 //eor-buffer
.var buffer = $a3 //presumably the tape won"t be running
.var x1 = $fb //points for drawing a line
.var y1 = $fc //these zero page addresses
.var x2 = $fd //don"t conflict with basic
.var y2 = $fe
.var oldx = $fd
.var chunk = $fe
.var dx = $67 //this is shared with t1 below
.var dy = $68
.var temp1 = $fb //of course, could conflict with x1
.var temp2 = $fc //temporary variables
.var ztemp = $02 //used for buffer swap.  don"t touch.
.var z1 = $22 //used by math routine
.var z2 = $24 //don"t touch these either!
.var z3 = $26
.var z4 = $28
.var k = $b6 //constant used for hidden
   //surface detection - don"t touch
.var hide = $b5 //are surfaces hidden?
.var fill = $50 //are we using eor-fill?
.var angmax = 120 //there are 2*pi/angmax angles

// vic

.var vmcsb = $d018
.var bkgnd = $d020
.var border = $d021
.var sstart = 1344 //row 9 in screen memory at 1024


// kernal

.var chrout = $ffd2
.var getin = $ffe4

// some variables

.var globxmin = $3f //these are used in clearing the
.var globxmax = $40 //drawing (global) buffer
.var globymin = $41
.var globymax = $42
.var locxmin = $57 //these are used in clearing the
.var locxmax = $58 //eor (local) buffer
.var locymin = $59
.var locymax = $60
.var p1x = $92 //these are temporary storage
.var p1y = $93 //used in plotting the projection
.var p1z = $94
.var p2x = $95 //they are here so that we
.var p2y = $96 //don"t have to recalculate them.
.var p2z = $ae
.var p3x = $af //they make life easy.
.var p3y = $b0
.var p3z = $b1 //why are you looking at me like that?
.var p1t = $b2 //don"t you trust me?
.var p2t = $b3
.var p3t = $b4 //having another child wasn"t my idea.
.var index = $51
.var countpts = $52
.var zoom = $71 //zoom factor
.var dsx = $61 //dsx is the increment for
   //rotating around x
.var dsy = $62 //similar for dsy, dsz
.var dsz = $63
.var sx = $64 //these are the actual angles in x y and z
.var sy = $65
.var sz = $66
.var t1 = $67 //these are used in the rotation
.var t2 = $68
.var t3 = $69 //see the article for more details
.var t4 = $6a
.var t5 = $6b
.var t6 = $6c
.var t7 = $6d
.var t8 = $6e
.var t9 = $6f
.var t10 = $70
.var a11 = $a5 //these are the elements of the rotation matrix
.var b12 = $a6 //xyz
.var c13 = $a7
.var d21 = $a8 //the number denotes (row,column)
.var e22 = $a9
.var f23 = $aa
.var g31 = $ab
.var h32 = $ac
.var i33 = $ad


//** macros

.macro move (a,b){
 lda a
 sta b
}

.macro getkey() {  //wait for a keypress
wait: jsr getin
 cmp #00
 beq wait
}

.macro debug (){  //print a character
	break()
}

.macro debuga(){
	break()
}

//-------------------------------

 lda #$00
 sta bkgnd
 sta border
 lda vmcsb
 and #%00001111 //screen memory to 1024
 ora #%00010000
 sta vmcsb

 ldy #00
 lda #<ttext
 sta temp1
 lda #>ttext
 sta temp2
 jmp title
ttext:.byte $93,$05,$11,$11,$11 //clear screen, white, crsr dn
  .text"             cube3d v3.2"
  .byte $0d, $0d
  .text"                  by"
  .byte $0d
.byte $9f //cyan
 .text"    stephen judd"
.byte $99
  .text"    george taylor"
  .byte $0d, $0d
.byte $9b
  .text"  check out the jan. 95 issue of"
  .byte $0d
.byte $96
 .text"  c=hacking"
.byte $9b
 .text" for more details!"
 .byte $0d
.byte $0d,$1d,$1d,$9e,$12
  .text"f1/f2"
  .byte $92
  .text" - inc/dec x-rotation"
  .byte $0d
.byte $1d,$1d,$12
  .text"f3/f4"
  .byte $92
  .text" - inc/dec y-rotation"
  .byte $0d
.byte $1d,$1d,$12
  .text"f5/f6"
  .byte $92
  .text" - inc/dec z-rotation"
  .byte $0d
.byte $1d,$1d,$12
  .text" f7  "
  .byte $92
  .text" - reset"
  .byte $0d
.byte $1d,$1d,$12
 .text" +/- "
 .byte $92
  .text" - zoom in/out"
  .byte $0d
.byte $1d,$1d,$12
  .text"  h  "
  .byte $92
  .text" - toggle hidden surfaces"
  .byte $0d
.byte $1d,$1d,$12
  .text"space"
  .byte $92
  .text" - toggle surface filling"
  .byte $0d, $0d
  .text"  press q to quit"
  .byte $0d
.byte $0d,$05 
  .text"      press any key to begin"
  .byte $0d
.byte $00
title: lda (temp1),y
 beq cont 
 jsr chrout
 iny
 bne title
 inc temp2
 jmp title
cont:  getkey()

//*** set up tables(?)

// tables are currently set up in basic
// and by the assembler.

tables: lda #>tmath1
 sta z1+1
 sta z2+1
 lda #>tmath2
 sta z3+1
 sta z4+1

//*** clear screen and set up "bitmap"
setup: lda #$01 //white
 sta $d021 //this is done so that older
 lda #147 //machines will set up
 jsr chrout
 lda #$00 //correctly
 sta $d021
 lda #<sstart
 adc #12 //the goal is to center the graphics
 sta temp1 //column 12
 lda #>sstart //row 9
 sta temp1+1 //sstart points to row 9
 lda #00
 ldy #00
 ldx #00 //x will count 16 rows for us
 clc

loop: sta (temp1),y
 iny
 adc #16
 bcc loop 
 clc
 lda temp1
 adc #40 //need to add 40 to the base pointer
 sta temp1 //to jump to the next row
 lda temp1+1
 adc #00 //take care of carries
 sta temp1+1
 ldy #00
 inx
 txa  //x is also an index into the character number
 cpx #16
 bne loop //need to do it 16 times

//*** clear buffers

 lda #<buff1
 sta buffer
 lda #>buff1
 sta buffer+1
 ldy #$00
 ldx #24 //assuming all three buffers are
 lda #$00 //back-to-back
bloop: sta (buffer),y
 iny
 bne bloop 
 inc buffer+1
 dex
 bne bloop 

//*** set up buffers

 lda #<buff1
 sta buffer
 lda #>buff1
 sta buffer+1
 sta ztemp //ztemp will make life simple for us
 lda vmcsb
 and #%11110001 //start here so that swap buffers will work right
 ora #%00001110
 sta vmcsb

//*** set up initial values

init: lda #00
 sta locxmin
 sta locxmax
 sta locymin
 sta locymax
 sta globxmin
 sta globymin
 sta globxmax
 sta globymax
 sta dsx
 sta dsy
 sta dsz
 sta sx
 sta sy
 sta sz
 sta fill
 lda #01
 sta hide
 lda #64
 sta zoom

//-------------------------------
// main loop

//*** get keypress

main: cli
kpress: jsr getin
 cmp #133 //f1?
 bne f2 
 lda dsx
 cmp #angmax/2 //no more than pi
 beq cont1 
 inc dsx //otherwise increase x-rotation
 jmp cont 
f2: cmp #137 //f2?
 bne f3 
 lda dsx
 beq cont1 
 dec dsx
 jmp cont 
f3: cmp #134
 bne f4 
 lda dsy
 cmp #angmax/2
 beq cont1 
 inc dsy //increase y-rotation
 jmp cont 
f4: cmp #138
 bne f5 
 lda dsy
 beq cont1 
 dec dsy
 jmp cont 
f5: cmp #135
 bne f6 
 lda dsz
 cmp #angmax/2
 beq cont1 
 inc dsz //z-rotation
 jmp cont 
f6: cmp #139
 bne f7 
 lda dsz
 beq cont1 
 dec dsz
 jmp cont 
f7: cmp #136
 bne plus 
 jmp init
cont1: jmp cont 
plus: cmp #"+"
 bne minus 
 inc zoom //bah, who needs error checking?
 inc zoom
 jmp cont 
minus: cmp #"-"
 bne h 
 dec zoom
 dec zoom
 bpl cont 
 inc zoom
 inc zoom
 jmp cont 
h: cmp #"h"
 bne space 
 lda hide
 eor #$01
 sta hide
 jmp cont 
space: cmp #" "
 bne q 
 lda fill
 eor #$01
 sta fill
 jmp cont 
q: cmp #"q" //q quits
 bne cont 
 jmp cleanup

cont: sei  //speed things up a bit

//*** update angles

update: clc
 lda sx
 adc dsx
 cmp #angmax //are we >= maximum angle?
 bcc cont1 
 sbc #angmax //if so, reset
cont1: sta sx
 clc
 lda sy
 adc dsy
 cmp #angmax
 bcc cont2 
 sbc #angmax //same deal
cont2: sta sy
 clc
 lda sz
 adc dsz
 cmp #angmax
 bcc cont3 
 sbc #angmax
cont3: sta sz

//*** rotate coordinates

rotate: //** first, calculate t1,t2,...,t10

//* two macros to simplify our life
.macro adda (a,b){  //add two angles together
 clc
 lda a
 adc b
 cmp #angmax //is the sum > 2*pi?
 bcc done
 sbc #angmax //if so, subtract 2*pi
done}

.macro suba(a,b){  //subtract two angles
 sec
 lda a
 sbc b
 bcs done
 adc #angmax //oops, we need to add 2*pi
done}

//* now calculate t1,t2,etc.

 suba(sy,sz) 
 sta t1 //t1=sy-sz
 adda(sy,sz) 
 sta t2 //t2=sy+sz
 adda(sx,sz) 
 sta t3 //t3=sx+sz
 suba(sx,sz) 
 sta t4 //t4=sx-sz
 adda(sx,t2) 
 sta t5 //t5=sx+t2
 suba(sx,t1) 
 sta t6 //t6=sx-t1
 adda(sx,t1) 
 sta t7 //t7=sx+t1
 suba(t2,sx) 
 sta t8 //t8=t2-sx
 suba(sy,sx) 
 sta t9 //t9=sy-sx
 adda(sx,sy) 
 sta t10 //t10=sx+sy

// et voila!

//** next, calculate a,b,c,...,i

//* another useful little macro
.macro div2 (){  //divide a signed number by 2
//it is assumed that the number
 bpl pos //is in the accumulator
 clc
 eor #$ff //we need to un-negative the number
 adc #01 //by taking it"s complement
 lsr  //divide by two
 clc
 eor #$ff
 adc #01 //make it negative again
 jmp donediv
pos: lsr  //number is positive
donediv:
}

.macro mul2 (){  //multiply a signed number by 2
 bpl posm
 clc
 eor #$ff
 adc #$01
 asl
 clc
 eor #$ff
 adc #$01
 jmp donemul
posm: asl
donemul:
}

//* note that we are currently making a minor leap
//* of faith that no overflows will occur.

calca: clc
 ldx t1
 lda cos,x
 ldx t2
 adc cos,x
 sta a11 //a=(cos(t1)+cos(t2))/2
calcb: ldx t1
 lda sin,x
 sec
 ldx t2
 sbc sin,x
 sta b12 //b=(sin(t1)-sin(t2))/2
calcc: ldx sy
 lda sin,x
  mul2()
 sta c13 //c=sin(sy)
calcd: sec
 ldx t8
 lda cos,x
 ldx t7
 sbc cos,x
 sec
 ldx t5
 sbc cos,x
 clc
 ldx t6
 adc cos,x //di=(cos(t8)-cos(t7)+cos(t6)-cos(t5))/2
  div2()
 clc
 ldx t3
 adc sin,x
 sec
 ldx t4
 sbc sin,x
 sta d21 //d=(sin(t3)-sin(t4)+di)/2
calce: sec
 ldx t5
 lda sin,x
 ldx t6
 sbc sin,x
 sec
 ldx t7
 sbc sin,x
 sec
 ldx t8
 sbc sin,x //ei=(sin(t5)-sin(t6)-sin(t7)-sin(t8))/2
  div2()
 clc
 ldx t3
 adc cos,x
 clc
 ldx t4
 adc cos,x
 sta e22 //e=(cos(t3)+cos(t4)+ei)/2
calcf: ldx t9
 lda sin,x
 sec
 ldx t10
 sbc sin,x
 sta f23 //f=(sin(t9)-sin(t10))/2
calcg: ldx t6
 lda sin,x
 sec
 ldx t8
 sbc sin,x
 sec
 ldx t7
 sbc sin,x
 sec
 ldx t5
 sbc sin,x //gi=(sin(t6)-sin(t8)-sin(t7)-sin(t5))/2
  div2()

 clc
 ldx t4
 adc cos,x
 sec
 ldx t3
 sbc cos,x
 sta g31 //g=(cos(t4)-cos(t3)+gi)/2
calch: clc
 ldx t6
 lda cos,x
 ldx t7
 adc cos,x
 sec
 ldx t5
 sbc cos,x
 sec
 ldx t8
 sbc cos,x //hi=(cos(t6)+cos(t7)-cos(t5)-cos(t8))/2
  div2()
 clc
 ldx t3
 adc sin,x
 clc
 ldx t4
 adc sin,x
 sta h32 //h=(sin(t3)+sin(t4)+hi)/2
whew: clc
 ldx t9
 lda cos,x
 ldx t10
 adc cos,x
 sta i33 //i=(cos(t9)+cos(t10))/2

//* it"s all downhill from here.

downhill: //*** clear buffer
// a little macro

.macro setbuf(){  //put buffers where they can be hurt
 lda #00
 sta buffer
 lda ztemp //high byte
stabuf: sta buffer+1
}

  setbuf()
clrdraw: ldx #08
 lda #00
fool: ldy #00
dope: sta (buffer),y
 iny
 bne dope 
 inc buffer+1
 dex
 bne fool 

//*** my goodness but i"m a dope
//clrdraw lda globxmin
// lsr  //need to get into the right column
// bcc even //explained in more detail below
// ldy #$80
// sty buffer //presumably this will be a little
// clc  //more efficient.
//:even adc buffer+1
// sta buffer+1
// lda globxmax
// sec
// sbc globxmin
// tax
// inx
// ldy globymax
// beq reset 
//:yay lda #$00
// ldy globymax
//:blah sta (buffer),y
// dey
// cpy globymin
// bcs blah 
// lda buffer
// eor #$80
// sta buffer
// bne whopee 
// inc buffer+1
//:whopee dex
// bne yay 
//:reset lda #0 //need to reset these guys
// sta globxmax
// sta globymax
// lda #$ff
// sta globxmin
// sta globymin

//*** next, read and draw polygons

readdraw: ldy #00
 sty index
objloop: ldy index
 lda polylist,y //first, the number of points
 bne cont //but if numpoints is zero then
 jmp objdone //we are at the end of the list
cont: sta countpts
 inc index

// rotate project and draw the polygon
// make sure buffer being drawn to is clear!

doit: jsr rotproj

// convert xmin and xmax to columns

 lda locxmin
 lsr
 lsr
 lsr  //x mod 8
 sta locxmin
 cmp globxmin
 bcs nah 
 sta globxmin
nah: lda locymin
 cmp globymin
 bcs uhuh 
 sta globymin
uhuh: lda locxmax
 lsr
 lsr
 lsr
 sta locxmax
 cmp globxmax
 bcc noway 
 sta globxmax
noway: lda locymax
 cmp globymax
 bcc eorfill
 sta globymax

// if using the eor-buffer, copy into drawing buffer
// and then clear the eor-buffer

eorfill: lda fill
 beq objloop

  setbuf()
 lda #<eorbuf
 sta temp1
 lda #>eorbuf
 sta temp1+1

 lda locxmin //locxmin now contains column
 lsr  //each column is 128 bytes
 bcc even //so there might be a carry
 ldy #$80
 sty buffer
 sty temp1
 clc
even: sta t2
 adc buffer+1
 sta buffer+1 //each column is 128 bytes
 lda t2
 adc temp1+1 //now we will start at the
 sta temp1+1 //column

 lda locxmax
 sec
 sbc locxmin
 tax //total number of columns to do
 inx  //e.g. fill columns 1..3
 ldy locymax
 bne foop 
 inc locymax
foop: ldy locymax
 lda #00
goop: eor (temp1),y //eor-buffer
 pha
// maybe put an eor below?
 eor (buffer),y
 sta (buffer),y
 lda #00 //might as well clear it now
 sta (temp1),y
 pla
 dey
 cpy locymin
 bcs goop 
 lda buffer
 eor #$80
 sta buffer
 sta temp1
 bne boop 
 inc buffer+1
 inc temp1+1
boop: dex
 bne foop 
 jmp objloop

objdone: //*** swap buffers

swapbuf: lda vmcsb
 eor #$02 //pretty tricky, eh?
 sta vmcsb
 lda #$08
 eor ztemp //ztemp=high byte just flips
 sta ztemp //between $30 and $38

 jmp main //around and around we go...

 .text"gee brain, what do you want to do "
 .text"tonight?"

//* rotate, project, and store the points
//
// this part is a significant change since
// v2.0.  now it is a completely general polygon plotter.
// a set of points is read in, rotated and projected, and
// plotted into the drawing buffer (eor or normal).

rotproj: // a neat macro
.macro neg(a){  //change the sign of a two"s complement
 clc
 lda a //number.
 eor #$ff
 adc #$01
}

//-------------------------------
// these macros replace the previous projection
// subroutine.

.macro smult() { //multiply two signed 8-bit
 //numbers: a*y/64 -> a
 sta z3
 clc  //this multiply is for normal
 eor #$ff //numbers, i.e. x=-64..64
 adc #$01
 sta z4
 lda (z3),y
 sec
 sbc (z4),y
}  //all done :)

.macro smultz(){ //multiply two signed 8-bit
   //numbers: a*y/64 -> a
 sta z1
 clc  //and this multiply is specifically
 eor #$ff //for the projection part, where
 adc #$01 //numbers are -110..110 and 0..40
 sta z2
 lda (z1),y
 sec
 sbc (z2),y
}  //all done :)

.macro project(a,b,c){  //the actual projection routine
//the routine takes the point
//]1 ]2 ]3, rotates and
//projects it, and stores the
//result in ]1 ]2 ]3.

 ldy a //multiply first rotation column
 lda a11
  smult()
 sta p1t
 lda d21
  smult()
 sta p2t
 lda g31
  smult()
 sta p3t
 ldy b //second column
 lda b12
  smult()
 clc
 adc p1t
 sta p1t
 lda e22
  smult()
 clc
 adc p2t
 sta p2t
 lda h32
  smult()
 clc
 adc p3t
 sta p3t
 ldy c //third column
 lda c13
  smult()
 clc
 adc p1t
 sta p1t
 lda f23
  smult()
 clc
 adc p2t
 sta p2t
 lda i33
  smult()
 clc
 adc p3t
 sta c //rotated z
 tax
 ldy zdiv,x //table of d/(z+z0)
   //now y contains projection const

 lda p1t
  smultz()
 ldx zoom
 cpx #64
 beq contx
 sty temp1
 ldy zoom
  smult()
 ldy temp1
contx: clc
 adc #64 //offset the coordinate
 sta a //rotated and projected
 cmp locxmin //see if it is a local minimum
 bcs notxmin
 sta locxmin
notxmin: cmp locxmax
 bcc notxmax
 sta locxmax

notxmax: lda p2t
  smultz()
 cpx #64
 beq conty
 ldy zoom
  smult()
conty: clc
 adc #64
 sta b //rotated and projected y
 cmp locymin
 bcs notymin
 sta locymin
notymin: cmp locymax
 bcc notymax
 sta locymax

notymax:
}  //all done

// lda #<eorbuf //first we need to clear the
// sta buffer //eor buffer
// lda #>eorbuf
// sta buffer+1

 lda #0 //reset ymin and ymax
 sta locymax
 sta locxmax
 lda #$ff
 sta locymin
 sta locxmin

readpts: ldy index
 lda polylist,y
 sta p1x
 iny
 lda polylist,y
 sta p1y
 iny
 lda polylist,y
 sta p1z
 iny
 dec countpts
 lda polylist,y
 sta p2x
 iny
 lda polylist,y
 sta p2y
 iny
 lda polylist,y
 sta p2z
 iny
 dec countpts
 lda polylist,y
 sta p3x
 iny
 lda polylist,y
 sta p3y
 iny
 lda polylist,y
 sta p3z
 iny
 sty index
 project(p1x,p1y) //p1z
 project(p2x,p2y) //p2z
 project(p3x,p3y) //p3z

 lda hide
 beq doit 
 lda p2x //hidden face check
 sec
 sbc p1x
 tay  //y=(x2-x1)
 lda p3y
 sec
 sbc p2y //a=(y3-y2)
  smult()
 sta temp1
 lda p3x
 sec
 sbc p2x
 tay
 lda p2y
 sec
 sbc p1y
  smult()
 cmp temp1 //if x1*y2-y1*x2 > 0 then face
 bmi doit //is visible
 dec countpts //otherwise read in remaining
 beq abort //points and return
poop: inc index
 inc index
 inc index
 dec countpts
 bne poop 
abort: rts

doit: lda p1x
 sta x1
 lda p1y
 sta y1
 lda p2x
 sta x2
 lda p2y
 sta y2
 jsr draw
 lda p2x
 sta x1
 lda p2y
 sta y1
 lda p3x
 sta x2
 lda p3y
 sta y2
 jsr draw

 dec countpts
 bne polyloop //is it just a triangle?
 jmp polydone

polyloop: ldy index
 lda polylist,y
 sta p2x
 iny
 lda polylist,y
 sta p2y
 iny
 lda polylist,y
 sta p2z
 iny
 sty index
 project(p2x,p2y) //p2z

 lda p2x
 sta x1
 lda p2y
 sta y1
 lda p3x
 sta x2
 lda p3y
 sta y2
 jsr draw

 lda p2x
 sta p3x
 lda p2y
 sta p3y
 dec countpts
 beq polydone
 jmp polyloop
polydone: lda p1x //close the polygon
 sta x2
 lda p1y
 sta y2
 lda p3x
 sta x1
 lda p3y
 sta y1
 jsr draw
 rts

 .text"same thing we do every night, pinky: "
 .text"try to take over the world!"


//-------------------------------
// general questionable-value error procedure

//choke ldx #00
//:loop lda :ctext,x
// beq done 
// jsr chrout
// inx
// jmp loop 
//:done rts
//:ctext.byte $0d //cr
// txt "something choked :("
//.byte $0d,$00 
//
 .text"narf!"

//-------------------------------
// drawin" a line.  a fahn lahn.

//** some useful macros

.macro cinit (a) {  //macro to initialize the counter
 lda a //dx or dy
 lsr
}  //the dx/2 makes a nicer looking line

//**** macro to take a step in x

.macro xstep (dx){
 ldx dx //number of loop iterations
  cinit(dx)
xloop: lsr chunk
 beq fixc //update column
 sbc dy
 bcc fixy //time to step in y
 dex
 bne xloop
done: lda oldx //plot the last chunk
 eor chunk
 ora (buffer),y
 sta (buffer),y
 rts

fixc: pha
 lda oldx
 ora (buffer),y //plot
 sta (buffer),y
 lda #$ff //update chunk
 sta oldx
 sta chunk
 lda #$80 //increase the column
 eor buffer
 sta buffer
 bne c2
 inc buffer+1
c2: pla
 sbc dy
 bcs cont
 adc dx
 .if (a=="i") { //do we use iny or dey?
 	iny
} else {
 dey
 }

cont: dex
 bne xloop
 jmp done

fixy: adc dx
 pha
 lda oldx
 eor chunk
 ora (buffer),y
 sta (buffer),y
 lda chunk
 sta oldx
 pla
 .if (a=="i") { //update y
 	iny
 }else {
 	dey
 }
 dex
 bne xloop
 rts
}  //end of macro xstep

//**** take a step in y

.macro ystep (a) {
 ldx dy //number of loop iterations
 beq done //if dy=0 it"s just a point
  cinit(dy)
 sec
yloop: pha
 lda oldx
 ora (buffer),y
 sta (buffer),y
 pla
 .if (a=="i") {
 	iny
 }else{
 	dey
 }
 sbc dx
 bcc fixx
 dex
 bne yloop
done: lda oldx
 ora (buffer),y
 sta (buffer),y
 rts

fixx: adc dy
 lsr oldx
 sec  //important!
 beq fixc
 dex
 bne yloop
 jmp done

fixc: pha
 lda #$80
 sta oldx
 eor buffer
 sta buffer
 bne c2
 inc buffer+1
c2: pla
 dex
 bne yloop
 jmp done
}  //end of macro ystep

// take an x step in the eor buffer
// the sole change is to use eor instead of ora

.macro eorxstep(a){
 ldx dx //number of loop iterations
  cinit(dx)
xloop: lsr chunk
 beq fixc //update column
 sbc dy
 bcc fixy //time to step in y
 dex
 bne xloop
done: lda oldx //plot the last chunk
 eor chunk
 eor (buffer),y
 sta (buffer),y
 rts

fixc: pha
 lda oldx
 eor (buffer),y //plot
 sta (buffer),y
 lda #$ff //update chunk
 sta oldx
 sta chunk
 lda #$80 //increase the column
 eor buffer
 sta buffer
 bne c2
 inc buffer+1
c2: pla
 sbc dy
 bcs cont
 adc dx
 .if (a=="i") {//do we use iny or dey?
 	iny
 }else {
 	dey
 }

cont: dex
 bne xloop
 jmp done

fixy: adc dx
 pha
 lda oldx
 eor chunk
 eor (buffer),y
 sta (buffer),y
 lda chunk
 sta oldx
 pla
 .if(a=="i") { //update y
 	iny
 }else{
 	dey
 	}
 dex
 bne xloop
 rts
}  //end of macro xstep


// take a y-step in the eor-buffer
// changes from above are: only plot last part of each
// vertical chunk, don"t plot last point, plot with eor

.macro eorystep (a){
 ldx dy //number of loop iterations
 beq done //if dy=0 it"s just a point
  cinit(dy)
 sec
//yloop pha
// lda oldx
// ora (buffer),y
// sta (buffer),y
// pla
yloop: 
  .if(a=="i") {
 	iny
 }else{
 	dey
 }
 sbc dx
 bcc fixx
 dex
 bne yloop
//done lda oldx
// ora (buffer),y
// sta (buffer),y
done: rts

fixx: adc dy
 pha  //we only plot the last part of each chunk
 lda oldx
 eor (buffer),y
 sta (buffer),y
 pla
 lsr oldx
 sec  //important!
 beq fixc
 dex
 bne yloop
 jmp done

fixc: pha
 lda #$80
 sta oldx
 eor buffer
 sta buffer
 bne c2
 inc buffer+1
c2: pla
 dex
 bne yloop
 jmp done
}  //end of macro ystep
//*** initial line setup

//* the commented lines below are now taken care of by the
//* calling routine.
//draw move(tx1,x1)   //move stuff into zero page
// move(tx2,x2)   //where it can be modified
// move(ty1,y1) 
// move(ty2,y2) 

draw: lda fill
 bne seteor 
  setbuf()
 jmp setup 
seteor: lda #<eorbuf //use eor buffer instead of
 sta buffer //display buffer for drawing
 lda #>eorbuf
 sta buffer+1

setup: sec  //make sure x1<x2
 lda x2
 sbc x1
 bcs cont 
 lda y2 //if not, swap p1 and p2
 ldy y1
 sta y1
 sty y2
 lda x1
 ldy x2
 sty x1
 sta x2

 sec
 sbc x1 //now a=dx
cont: sta dx
 ldx x1 //put x1 into x, now we can trash x1

column: txa //find the first column for x
 lsr
 lsr  //there are x1/8 128 byte blocks
 lsr  //which means x1/16 256 byte blocks
 lsr
 bcc even //with a possible extra 128 byte block
 ldy #$80 //if so, set the high bit
 sty buffer
 clc
even: adc buffer+1 //add in the number of 256 byte blocks
 sta buffer+1

 sec
 lda y2 //calculate dy
 sbc y1
 bcs cont2 //is y2>y1?
 eor #$ff //otherwise dy=y1-y2
 adc #$01
cont2: sta dy
 cmp dx //who"s bigger: dy or dx?
 bcc stepinx //if dx, then...
 jmp stepiny

stepinx: ldy y1
 cpy y2
 lda bitp,x //x currently contains x1
 sta oldx
 sta chunk
 bcc xincy //do we step forwards or backwards in y?
 jmp xdecy

xincy: lda fill
 beq normxinc
  eorxstep(),iny
normxinc:  xstep(),iny

xdecy: lda fill
 beq normxdec
  eorxstep(),dey
normxdec:  xstep(),dey

stepiny: ldy y1
 lda bitp,x //x=x1
 sta oldx
 lsr  //y doesn"t use chunks
 eor oldx //so we just want the bit
 sta oldx
 cpy y2
 bcs ydecy

yincy: lda fill
 beq norminc
  eorystep(),iny
norminc:  ystep(),iny

ydecy: lda fill
 beq normdec
  eorystep(),dey
normdec:  ystep(),dey


//-------------------------------
// clean up

cleanup: lda vmcsb //switch char rom back in
 and #%11110101 //default
 sta vmcsb

 rts  //bye!

 .text"spinal cracker "
 .text"slj 6/95"

//-------------------------------
// set up bit table

.align $00
 //ds ^ //clear to end of page
   //so that tables start on a page boundary
    //128 entries for x
bitp: .fill 16, %11111111, %01111111, %00111111, %00011111, %00001111, %00000111, %00000011, %00000001
 
sin: //table of sines, 120 bytes
.var cos = sin+128 //table of cosines
   //both of these trig tables are
   //currently set up from basic
.var zdiv = cos+128 //division table
.var tmath1 = zdiv+384 //math table of f(x)=x*x/256
.var tmath2 = tmath1+512 //second math table
.var polylist = tmath2+512 //list of polygons
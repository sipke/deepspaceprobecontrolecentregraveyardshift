;--------------------------
;MATHS
;some math macros
;--------------------------

;angles
t1      = $0a
t2      = $0b
t3      = $0c
t4      = $0d
t5      = $0e
t6      = $0f
t7      = $10
t8      = $11
t9      = $12
t10     = $13

;matrix
A     = $14
B     = $15
C     = $16
D     = $17
E     = $18
F     = $19
G     = $1a
H     = $1b
I     = $1c

;arithmetic shift right
!macro asr {
         ;copy bit 7 to carry
         cmp #$80
         ;rotate right -> if negative, bit 7 is set again, else it is cleared -> arithmetic shift right
         ror
}

;--------------------------
;MULTIPLICATIONS
;
;--------------------------

!macro adda .a, .b {
         adc .a
         ;>255 -> in any case do a subtract
         bcs ++
         cmp #180
         ;>180 and < 256 -> so subtract
         bcc +
++
         sbc #180
         clc
+
         sta .b
}

!macro suba .a, .b {
         sbc .a
         bcs +
         adc #180
+
         sta .b
}

!macro mul_ay_set_a1 {
         sta z1
         eor #$ff
         sta z2
}

!macro mul_ay_set_a2 {
         sta z3
         eor #$ff
         sta z4
}

!macro mul_ay_set_a3 {
         sta z5
         eor #$ff
         sta z6
}

!macro mul_ay_set_a4 {
         sta z7
         eor #$ff
         sta z8
}

;--------------------------
;PRE PROJECT
;some general calculations that are the same for all transformed vertices
;see the c= hacking issue #8-10 (Judd) for more details. All what you see here is more or less the same, just optimized a bit
;--------------------------

!macro fetch_sin_cos {
         ldx deg_x
         cpx #180
         bcc +
         ldx #$00
         stx deg_x
+
         lda deg_z
         cmp #180
         bcc +
         lda #$00
         sta deg_z
+
         ldy deg_y
         cpy #180
         bcc +
         ldy #$00
         sty deg_y
         clc
+
         tya
         +adda deg_z, t2    ;14
         sec
         +suba deg_x, t8    ;9,5

         tya
         +suba deg_x, t9    ;9,5
         tya
         +suba deg_z, t1    ;9,5

         txa
         +suba deg_z, t4    ;9,5
         txa
         +suba t1, t6       ;9,5

         clc
         txa
         +adda deg_z, t3    ;14
         txa
         +adda t2, t5       ;14
         txa
         +adda t1, t7       ;14
         txa
         +adda deg_y, t10   ;14

                            ;139,5

         ;sin(y)
         lda sinus,y
         clc
         adc sinus,y
         sta C              ;13

         ldx t1
         ldy t2             ;6

         ;(cos(t1) + cos(t2)) / 2
         lda cosinus,x
         clc
         adc cosinus,y
         sta A

         ;(sin(t1) - sin(t2)) / 2
         lda sinus,x
         sec
         sbc sinus,y
         sta B

         ldx t9
         ldy t10

         ;(sin(t9) - sin(t10)) / 2
         lda sinus,x
         sec
         sbc sinus,y
         sta F

         ;(cos(t9) + cos(t10)) / 2
         lda cosinus,x
         clc
         adc cosinus,y
         sta I

         ldx t3
         ldy t4

         ;(sin(t3) - sin(t4)) / 2
         lda sinus,x
         sec
         sbc sinus,y
         sta D

         ;(sin(t3) + sin(t4)) / 2
         lda sinus,x
         clc
         adc sinus,y
         sta H

         ;(cos(t3) + cos(t4)) / 2
         lda cosinus,x
         clc
         adc cosinus,y
         sta E

         ;(cos(t4) - cos(t3)) / 2
         lda cosinus,y
         sec
         sbc cosinus,x
         sta G
;135
         ldx t5
         ldy t6

         ;tmp2 = sin(t5) - sin(t6)
         lda sinus,x
         sec
         sbc sinus,y
         sta tmp2

         ;(cos(t6) - cos(t5) ...
         lda cosinus,y
         sec
         sbc cosinus,x
         sta tmp1

         ldx t7
         ldy t8

         ;... -cos(t7) + cos(t8)) / 4 + Da
         sec
         sbc cosinus,x
         clc
         adc cosinus,y
         +asr
         clc
         adc D
         sta D

         ;... +cos(t7) - cos(t8)) / 4 + H
         lda tmp1
         clc
         adc cosinus,x
         sec
         sbc cosinus,y
         +asr
         clc
         adc H
         sta H
;224
         ;sin(t7) + sin(t8)
         lda sinus,x
         clc
         adc sinus,y
         sta tmp1

         ;... -tmp1 + (sin(t5) - sin(t6)) / 4 + E
         eor #$ff
         sec
         adc tmp2
         +asr
         clc
         adc E
         sta E

         ;(sin(t6) - sin(t5) - tmp1 / 4 + G
         lda #$00
         sec
         sbc tmp2
         sec
         sbc tmp1
         +asr
         clc
         adc G
         sta G                       ;280
}

;--------------------------
;PROJECT
;now work out the 3x3 matrix
;--------------------------

!macro project ~.vertx, ~.verty, ~.vertz {

         ;if you want to zoom, just apply another multiplication when loading vertx/y/z to scale down

.vertx   lda $1000,x
         +mul_ay_set_a1
.verty   lda $1000,x
         +mul_ay_set_a2
.vertz   lda $1000,x
         +mul_ay_set_a3 ;36

         ;multiply all for z
         ldy G
         lda (z1),y
         sec
         sbc (z2),y
         ldy H
         clc
         adc (z3),y
         sec
         sbc (z4),y
         ldy I
         clc
         adc (z5),y
         sec
         sbc (z6),y    ;49

         ;now we have final z and can get perspective factor and setup multiplicator beforehand
         tay
         lda fact_z,y
         +mul_ay_set_a4 ;14

         ;do all muls for x
         ldy A
         lda (z1),y
         sec
         sbc (z2),y
         ldy B
         clc
         adc (z3),y
         sec
         sbc (z4),y
         ldy C
         clc
         adc (z5),y
         sec
         sbc (z6),y   ;49
         tay
         ;perspective x
         lda (z7),y
         ;add offset for x, as the filler only handles unsigned values (values in math table are all > $0 and < $40)
         ora #$40
         sec
         sbc (z8),y
         sta vertices_x,x  ;21

         ;do all muls for y
         ldy D
         lda (z1),y
         sec
         sbc (z2),y
         ldy E
         clc
         adc (z3),y
         sec
         sbc (z4),y
         ldy F
         clc
         adc (z5),y
         sec
         sbc (z6),y    ;49
         tay
         ;perspective y
         lda (z7),y
         ;add offset to y, as the filler only handles unsigned values (values in math table are all > $0 and < $40)
         ora #$40
         sec
         sbc (z8),y
         sta vertices_y,x  ;21

         ;239 cycles
}

meshtab
         !word mesh1, mesh2, mesh3, mesh4
;linetab
;         !word line1, line2, line3, line4

faces

face1
         ;facesA  B  C  D  P
         !byte 3, 2, 1, 0, 4
         !byte 4, 5, 6, 7, 4
         !byte 7, 3, 0, 4, 3
         !byte 2, 6, 5, 1, 3
         !byte 0, 1, 5, 4, 2
         !byte 7, 6, 2, 3, 1
face2
         ;faces A   B   C   D   P
         !byte  8,  0,  1,  8,  4
         !byte  9,  1,  2,  9,  4
         !byte 10,  2,  3, 10,  4
         !byte 11,  3,  0, 11,  4
         !byte 11,  4,  7, 11,  4
         !byte 10,  7,  6, 10,  4
         !byte  9,  6,  5,  9,  4
         !byte  8,  5,  4,  8,  4

         !byte  3,  2,  1,  0,  4
         !byte  4,  5,  6,  7,  4
         !byte  0,  8,  4,  11, 4
         !byte  10, 6,  9,  2,  4
         !byte  1,  9,  5,  8,  4
         !byte  3, 11,  7,  10, 4
face3
         ;faces A   B   C   D   P
         !byte  1,  3,  0,  1,  2
         !byte  1,  0,  2,  1,  2
         !byte  4,  1,  6,  5,  2
         !byte  4,  3,  1,  4,  3
         !byte  6,  1,  2,  6,  3
         !byte  3,  7,  8,  3,  3
         !byte  2,  10, 9,  2,  3
         !byte  7,  3,  4,  7,  2
         !byte  9,  6,  2,  9,  2
         !byte  27, 4,  6,  28, 3
         !byte  9,  10, 28, 6,  4
         !byte  4,  27, 8,  7,  4
         !byte  3,  8,  27, 0,  1
         !byte  28, 2,  0,  27, 1
         !byte  10, 29, 27, 28, 1
         !byte  12, 11, 14, 13, 4
         !byte  17, 18, 15, 16, 4
         !byte  20, 19, 22, 21, 4
         !byte  25, 26, 23, 24, 4
face4
         ;facesA   B   C   D   P   H   T/Q
         !byte 3,  4,  1,  0,  4
;         !byte 4,  5,  2,  1,  1
;         !byte 6,  7,  4,  3,  1
         !byte 7,  8,  5,  4,  3

         !byte 14, 13, 10, 11, 4
;         !byte 13, 12, 9,  10, 1
;         !byte 17, 16, 13, 14, 1
         !byte 16, 15, 12, 13, 3

         !byte 5,  19, 18, 2,  3
;         !byte 19, 14, 11, 18, 1
;         !byte 8,  20, 19, 5,  1
         !byte 20, 17, 14, 19, 2

         !byte 12, 22, 21, 9,  3
;         !byte 22, 3,  0,  21, 1
;         !byte 15, 23, 22, 12, 1
         !byte 23, 6,  3,  22, 2

         !byte 21, 24, 10, 9,  2
;         !byte 24, 18, 11, 10, 1
;         !byte 0,  1,  24, 21, 1
         !byte 1,  2,  18, 24, 1

         !byte 23, 25, 7,  6,  2
;         !byte 25, 20, 8,  7,  1
;         !byte 15, 16, 25, 23, 1
         !byte 16, 17, 20, 25, 1
face5

meshes
mesh1
         ;cube
         ;num vertices
         !byte 8
         ;backface culling
         !byte 1
         ;shaded
         !byte 0
         ;pointers to vertices
         !word vertx1
         !word verty1
         !word vertz1
         ;start pos of faces
         !byte face1-face1
         ;end pos of faces
         !byte face2-face1
         f1 = 37
mesh2
         ;see through thingy
         ;num vertices
         !byte 12
         ;backface culling
         !byte 1
         ;shaded
         !byte 1
         ;pointers to vertices
         !word vertx2
         !word verty2
         !word vertz2
         ;start pos of faces
         !byte face2-face1
         ;end pos of faces
         !byte face3-face1
         f2 = 44 ; was 47
mesh3
         ;elite spaceship
         ;num vertices
         !byte 30
         ;backface culling
         !byte 1
         ;shaded
         !byte 0
         ;pointers to vertices
         !word vertx3
         !word verty3
         !word vertz3
         ;start pos of faces
         !byte face3-face1
         ;end pos of faces
         !byte face4-face1
         f3 = 59
mesh4
         ;würfel
         ;num vertices
         !byte 26
         ;backface culling
         !byte 1
         ;shaded
         !byte 0
         ;pointers to vertices
         !word vertx4
         !word verty4
         !word vertz4
         ;start pos of faces
         !byte face4-face1
         ;end pos of faces
         !byte face5-face1
         f4 = 37

vertices
vertx1
         ;vertices_x
         !byte -f1,  f1,  f1, -f1, -f1,  f1,  f1, -f1
verty1
         ;vertices_y
         !byte -f1, -f1,  f1,  f1, -f1, -f1,  f1,  f1
vertz1
         ;vertices_z
         !byte -f1, -f1, -f1, -f1,  f1,  f1,  f1,  f1

vertx2
         ;vertices_x
         !byte   0,  f2,   0, -f2,   0,  f2,   0, -f2,  f2,  f2, -f2, -f2
verty2
         ;vertices_y
         !byte -f2, -f2, -f2, -f2,  f2,  f2,  f2,  f2,   0,   0,   0,   0
vertz2
         ;vertices_z
         !byte  f2,   0, -f2,   0,  f2,   0, -f2,   0,  f2, -f2, -f2,  f2

vertx3
         ;vertices_x
         !byte     00 * f3 / 100
         !byte     00 * f3 / 100
         !byte     60 * f3 / 100
         !byte    -60 * f3 / 100
         !byte    -17 * f3 / 100
         !byte     00 * f3 / 100
         !byte     17 * f3 / 100
         !byte    -90 * f3 / 100
         !byte    -100 * f3 / 100
         !byte     90 * f3 / 100
         !byte     100 * f3 / 100
         !byte    -06 * f3 / 100
         !byte    -06 * f3 / 100
         !byte    -34 * f3 / 100
         !byte    -34 * f3 / 100
         !byte     06 * f3 / 100
         !byte     06 * f3 / 100
         !byte     34 * f3 / 100
         !byte     34 * f3 / 100
         !byte    -75 * f3 / 100
         !byte    -60 * f3 / 100
         !byte    -60 * f3 / 100
         !byte    -60 * f3 / 100
         !byte     75 * f3 / 100
         !byte     60 * f3 / 100
         !byte     60 * f3 / 100
         !byte     60 * f3 / 100
         !byte    -20 * f3 / 100
         !byte     20 * f3 / 100
         !byte     60 * f3 / 100
verty3
         ;vertices_y
         !byte    -22 * f3 / 100
         !byte    -18 * f3 / 100
         !byte    -15 * f3 / 100
         !byte    -15 * f3 / 100
         !byte     00 * f3 / 100
         !byte     00 * f3 / 100
         !byte     00 * f3 / 100
         !byte     00 * f3 / 100
         !byte     00 * f3 / 100
         !byte     00 * f3 / 100
         !byte     00 * f3 / 100
         !byte    -12 * f3 / 100
         !byte     09 * f3 / 100
         !byte     06 * f3 / 100
         !byte    -08 * f3 / 100
         !byte    -12 * f3 / 100
         !byte     09 * f3 / 100
         !byte     06 * f3 / 100
         !byte    -08 * f3 / 100
         !byte    -01 * f3 / 100
         !byte    -06 * f3 / 100
         !byte    -01 * f3 / 100
         !byte     04 * f3 / 100
         !byte    -01 * f3 / 100
         !byte    -06 * f3 / 100
         !byte    -01 * f3 / 100
         !byte     04 * f3 / 100
         !byte     18 * f3 / 100
         !byte     18 * f3 / 100
         !byte    -15 * f3 / 100

vertz3
         of3 = 30
         ;vertices_z
         !byte    (of3 + -70) * f3 / 100
         !byte    (of3 +  10) * f3 / 100
         !byte    (of3 + -70) * f3 / 100
         !byte    (of3 + -70) * f3 / 100
         !byte    (of3 +  40) * f3 / 100
         !byte    (of3 +  40) * f3 / 100
         !byte    (of3 +  40) * f3 / 100
         !byte    (of3 + -40) * f3 / 100
         !byte    (of3 + -70) * f3 / 100
         !byte    (of3 + -40) * f3 / 100
         !byte    (of3 + -70) * f3 / 100
         !byte    (of3 + -70) * f3 / 100
         !byte    (of3 + -70) * f3 / 100
         !byte    (of3 + -70) * f3 / 100
         !byte    (of3 + -70) * f3 / 100
         !byte    (of3 + -70) * f3 / 100
         !byte    (of3 + -70) * f3 / 100
         !byte    (of3 + -70) * f3 / 100
         !byte    (of3 + -70) * f3 / 100
         !byte    (of3 + -70) * f3 / 100
         !byte    (of3 + -70) * f3 / 100
         !byte    (of3 + -70) * f3 / 100
         !byte    (of3 + -70) * f3 / 100
         !byte    (of3 + -70) * f3 / 100
         !byte    (of3 + -70) * f3 / 100
         !byte    (of3 + -70) * f3 / 100
         !byte    (of3 + -70) * f3 / 100
         !byte    (of3 + -70) * f3 / 100
         !byte    (of3 + -70) * f3 / 100
         !byte    (of3 + -70) * f3 / 100


vertx4
         ;vertices_x
         !byte -f4,  0,  f4
         !byte -f4,  0,  f4
         !byte -f4,  0,  f4

         !byte -f4,  0,  f4
         !byte -f4,  0,  f4
         !byte -f4,  0,  f4

         !byte  f4,  f4,  f4
         !byte -f4, -f4, -f4

         !byte  0
         !byte  0
verty4
         ;vertices_y
         !byte -f4, -f4, -f4
         !byte  0,  0,  0
         !byte  f4,  f4,  f4

         !byte -f4, -f4, -f4
         !byte  0,  0,  0
         !byte  f4,  f4,  f4

         !byte -f4,  0,  f4
         !byte -f4,  0,  f4

         !byte -f4
         !byte  f4
vertz4
         ;vertices_z
         !byte -f4, -f4, -f4
         !byte -f4, -f4, -f4
         !byte -f4, -f4, -f4

         !byte  f4,  f4,  f4
         !byte  f4,  f4,  f4
         !byte  f4,  f4,  f4

         !byte  0,  0,  0
         !byte  0,  0,  0

         !byte  0
         !byte  0

lines
;line1
;         ;lines
;         !byte 0, 1
;         !byte 1, 2
;         !byte 2, 3
;         !byte 3, 0
;         !byte 4, 5
;         !byte 5, 6
;         !byte 6, 7
;         !byte 7, 4
;         !byte 0, 4
;         !byte 1, 5
;         !byte 2, 6
;         !byte 3, 7
;
;line2
;         ;lines
;         !byte 8, 0
;         !byte 0, 1
;         !byte 1, 8
;
;         !byte 9, 1
;         !byte 1, 2
;         !byte 2, 9
;
;         !byte 10, 2
;         !byte 2, 3
;         !byte 3, 10
;
;         !byte 11, 3
;         !byte 3, 0
;         !byte 0, 11
;
;         !byte 11, 4
;         !byte 4, 7
;         !byte 7, 11
;
;         !byte 10, 7
;         !byte 7, 6
;         !byte 6, 10
;
;         !byte 9, 6
;         !byte 6, 5
;         !byte 5, 9
;
;         !byte 8, 5
;         !byte 5, 4
;         !byte 4, 8
;
;line3
;         !byte 0,1
;         !byte 0,2
;         !byte 0,3
;         !byte 0,27
;         !byte 1,2
;         !byte 1,3
;         !byte 1,4
;         !byte 1,6
;         !byte 1,10
;         !byte 2,6
;         !byte 2,9
;         !byte 2,28
;         !byte 3,4
;         !byte 3,7
;         !byte 3,8
;         !byte 4,5
;         !byte 4,6
;         !byte 4,7
;         !byte 4,27
;         !byte 5,6
;         !byte 6,9
;         !byte 6,28
;         !byte 6,29
;         !byte 7,8
;         !byte 8,27
;         !byte 9,10
;         !byte 10,28
;         !byte 10,29
;         !byte 11,12
;         !byte 11,14
;         !byte 12,13
;         !byte 13,14
;         !byte 15,18
;         !byte 15,16
;         !byte 16,17
;         !byte 17,18
;         !byte 19,20
;         !byte 19,22
;         !byte 20,21
;         !byte 21,22
;         !byte 23,26
;         !byte 23,24
;         !byte 24,25
;         !byte 25,26
;         !byte 27,29
;         !byte 27,28
;line4

#importonce

.import source "basic_functions.asm"
.import source "macros.asm"
.import source "constants.asm"
.import source "ingame_logic.asm" 

//----------- precalculate table of x coordinates of circle as function of y --------------------
//----------- just half is needed because of symmetry ---------------------------
// Y from 1 -> 63, 
//hard code Y  =0 and 64
.memblock "calculate circletable"
calculateCircleTable:
{
    ldx #0
    lda #0
!:  sta CIRCLE_X_TABLE,x
    sta CIRCLE_X_TABLE+256,x
    inx
    bne !-
    lda #0
    sta CIRCLE_X_TABLE
    lda #$81
    sta CIRCLE_X_TABLE + 64*5

//calc X for Y values    
// X = SQRT(128*Y-(Y*Y))/64    
// for y values of 0-64
loop:    
    lda #0
    ldy yTemp
    jsr FUNC_INT_TO_FAC1
    StoreFAC(FAC_TMP2)
    //fac contains Y
    MulFACExp2(7)
    //Fac = 128*y => tmp
    StoreFAC(FAC_TMP)
    //DivFACExp2(7)
    //Fac = fac/128 = y
   // SQUAREFAC1()      //doesn't work no idea why
   //
   //so now multitply tmp value by fac
    LoadFAC(FAC_TMP2)
    MultMemFAC(FAC_TMP2)
    //Fac = mem-Fac
    SubFACFromMem(FAC_TMP)
    jsr FUNC_SQRT
    //fac/64
    DivFACExp2(6)
sm1:

    ldx #<(CIRCLE_X_TABLE + 5)
    ldy #>(CIRCLE_X_TABLE + 5) 
    jsr STFAC

    lda sm1 +1
    clc
    adc #5
    sta sm1 + 1
    bcc !+
    inc sm1 + 3
!:
    ldy yTemp
    iny
    cpy #$40 
    beq !+
    sty yTemp   

    jmp loop
!:    
}
    rts    



.memblock "calculate view"
calculateView:

    //init start points to $ff, which means, no points
    lda #$ff
    ldx #0
!:  sta START_POINTS,x
    inx
    bne !-

//-------------------------------------------------------------

    //TODO: calculate time for signal
    //write time at photoEventReceivedAt


/*
// X value
//TODO; not hard coded X
    LoadFACInt(-3000)
    StoreFAC(FAC_X)
// angle of probe
//TODO not hard coded angle
    LoadFACInt(-1)
    lda #>FAC_ANGLE_RAD
    ldy #<FAC_ANGLE_RAD
    AddMemFAC(HALF)
    StoreFAC(FAC_ANGLE_RAD)
*/
//entry point for calculating projection of planet


    //x= x0 + v*t  point where signal is sent + distance probe has travelled in time the signal needs to get to probe
    //t = x/c  time the signal needs to get to probe is distance to probe (unknown) divided by c
    // x = x0+v*x/c
    //x-v*x/c = x0
    //(1-v/c)*x=x0

//these are the steps to do x= FAC_X

    //x=x0/(1-v/c)      1-v/c is a constant, we keep it in memory
    //x=x0/const    , we can store it in FAC_X again
    //t=x/c     //time in seconds, need to multiply by frames/sec times two, because it has to get back as well
    //tf = 2*50*x/c   //we need to add it to start time, but we'll do that after FAC calcs, because that is faster in 16bit

    //loadfac(facx)
    //divfac(const)
    //storefac(facx)
    //divfac(c)
    //multfac(hundred)
    //write 16 bits to temp storage, will retrieve after routine, must be written to indexed location

//take distance to planet at moment of sending signal
    LoadFAC(FAC_X)
    AddMemFAC(distanceEarthToPlanet)
    StoreFAC(distProbeToEarthFac)
    DivFACByMem(oneMinusVDivC)
    SubMemFromFAC(distanceEarthToPlanet)
    StoreFAC(FAC_X) //this is the distance 

    LoadFAC(distProbeToEarthFac)
    DivFACByMem(lightSpeedFAC)
    MultMemFAC(hundred)

    jsr FAC_TO_INT
    //should fit into 16 bits, so $64 an $65
    lda curPhotoHandling
    asl
    tax
    lda $64
    clc
    //should be adc photoInStructionSentAtTime + 1,x
    //adc frameCountHi
    adc photoInStructionSentAtTime + 1,x
    sta photoEventReceivedAt + 1,x
    lda $65
    adc photoInStructionSentAtTime,x
    //should be adc photoInStructionSentAtTime,x
    adc frameCountLo
    sta photoEventReceivedAt,x

//     d = Sqr(x * x + y * y)
    LoadFAC(FAC_X)  //x
    MultMemFAC(FAC_X)   //x*x
    AddMemFAC(ySquared)   //+y^2

    jsr FUNC_SQRT   //sqrt(x^2 + y ^2)
    StoreFAC(FAC_DISTANCE)

//         angleToPlanet = x<>0 ? Atn(y / x) : 0.5*PI 
    //if Exp FAC_X=0 -> FAC_X = 0
    lda FAC_X
    bne !+
    LoadFAC(HALF_PI)
    jmp endAtan
  
!:  LoadFAC(FAC_Y)
    DivFACByMem(FAC_X)
    jsr FUNC_ATAN_FAC1

    ldx #$00
    lda FAC_X + 1   //remember if x is negative
    bpl !+
    ldx #$ff
!:  stx INT_TMP4
    //SubMemFromFAC(PI)
endAtan:
    StoreFAC(FAC_ANGLE_PLANET_RAD)
    SubMemFromFAC(FAC_ANGLE_RAD)

    lda INT_TMP4
    bmi noCompensation  //if x is positive subtract PI from angle difference 
    SubMemFromFAC(PI)
noCompensation:
    StoreFAC(FAC_ANGLE_DIV)

//TODO this is commented out, because now it's not doing anything,, but why was it here anyway?    
    //LoadFAC(FAC_ANGLE_RAD)

//  s = sm * y / d
    LoadFAC(FAC_Y)
    DivFACByMem(FAC_DISTANCE)
    MultMemFAC(planetMaxSize)
    StoreFAC(FAC_PLANET_SIZE)
    //2 store half planet size
    DivFACExp2(1)
    StoreFAC(FAC_PLANET_SIZE_DIV2)

//  startY = w / 2 - s / 2
//because max size = screen size, no need to determine if top (startY) or bottom (endY) is outside of view
    LoadFAC(screenWidthDiv2)
    SubMemFromFAC(FAC_PLANET_SIZE_DIV2)
    StoreFAC(FAC_START_Y)

//add planet size for end Y (w/2-s/2+s= w/2+s/2), planet always vertically centered
    AddMemFAC(FAC_PLANET_SIZE)
    
    StoreFAC(FAC_END_Y) 
    
    jsr FAC_TO_INT
    lda $65
    sta endYInt
    LoadFAC(FAC_START_Y)
    jsr FAC_TO_INT
    lda $65
    sta startYInt

//  px = w / 2 - ((angleToPlanet - a) / (fov / 2)) * (w / 2) - s / 2
//  px = wdiv2 - ((anglediv) / (fovdiv2)) * (wdiv2) - planetsizediv2
//(anglediv) / (fovdiv2) -> fac
//fac* (wdiv2) -> fac
//wdiv2-fac -> fac
//fac -planetsizediv2 -> fac


//anglediv/fovdiv2 ->fac
    LoadFAC(FAC_ANGLE_DIV)
    //(anglediv) / (fovdiv2)
    DivFACByMem(FAC_FOV_DIV_2)
    //* (wdiv2)
    MultMemFAC(screenWidthDiv2)

//---------------    
    //wdiv2-fac
    SubFACFromMem(screenWidthDiv2)
    //fac-planetsizediv2
    SubMemFromFAC(FAC_PLANET_SIZE_DIV2)
//store fac as px
    StoreFAC(FAC_PLANET_X)
// calculate midpoint
    //px + s / 2  'midpoint
    AddMemFAC(FAC_PLANET_SIZE_DIV2)
    StoreFAC(FAC_MIDPOINT)
   

// screenwidth/planetwidth   
    LoadFAC(screenWidth)
    DivFACByMem(FAC_PLANET_SIZE)
    StoreFAC(FAC_WIDTH_FACTOR)

    
//=========================
//now calculate begin and endpoints and switchpoints (point where light part becomes dark)
//we do this from startY to centerY and calculate the distance from the midpoint of the planet to the endpoint, the half width of that line, hw
    //also calculate switchpoints
    //switchpoint is at width*factor, where  >=0 factor <=1
    //factor is aproximated by dividing distance to planet by a certain max 
    //using hw, the switchpoint is at hw*2*factor
    //now make integers of the numbers and store them, we now have 64 integers for hw (hwi) and 64 switchpoint integres spi, all fitting within a byte
    //the lower half is done by reverse copying the endpoint and switchpoints
    //by doing this when they're already byte values it will be MUCH quicker
    //starting points are calculated by subtracting midpoint value, then negate it and add midpoint


    //calc factor of switch from dark to light
    //f = (x+maxX)/(2*maxX)
    LoadFAC(FAC_X)
    AddMemFAC(maxX)
    DivFACByMem(maxX)
    DivFACExp2(1)
    StoreFAC(FAC_SWITCH_FACTOR)
    //checked: OK

    LoadFAC(FAC_START_Y)
    StoreFAC(FAC_TMP)
loop:    
//startPoint = midpoint - Round(Sheets("sine").Cells(Int(yp * (w / s)) + 1, 2) * (s / 2))

    LoadFAC(FAC_TMP)
    jsr $bc9b //QINT
    lda $65
    sta INT_Y
    cmp #$40
    bne !+
    jmp pointsDone
!:  LoadFAC(FAC_TMP)
    // calculate index
    // for width factor table
    SubMemFromFAC(FAC_START_Y)
    MultMemFAC(FAC_WIDTH_FACTOR)  
    jsr FUNC_INT    //convert to int
    MultMemFAC(five) //multiply by five to get right index in FAC table
     //OK till here
    jsr $bc9b //FUNC_INT    
    AddMemFAC(five)  //zero value has no use, really 
    
    lda $64 //high byte of int
    clc
    adc #>CIRCLE_X_TABLE //$98 //add high byte of start table, zero aligned
    tay
    lda $65
    jsr LDFAC   //read value from table


//calculate real startpoint
    //mulitply half planet size with factor, gives width of line
    MultMemFAC(FAC_PLANET_SIZE)
    StoreFAC(FAC_LINE_WIDTH)
    //calulate half of width
    DivFACExp2(1)
    //midpoint - halfwidth = startpoint
    StoreFAC(FAC_LINE_WIDTH_DIV2)
    SubFACFromMem(FAC_MIDPOINT)
    StoreFAC(FAC_REAL_STARTPOINT)
//  starting point smaller than or equal to -128 means there will be nothing showing
//  also if starting point is larger than 128 it will be out of view
//  so check if exponent means its  larger or equal to 128, so E>=7
//  we can ignore the line if it's that
    lda $61
    cmp #$88
    bcc !+
    jmp ignoreAndNext
//  otherwise: if it's negative: set starting point to zero
!:  lda $66
    bpl storeSP
    LoadFAC(zero)
    jmp storeSP
storeSP:
    StoreFAC(FAC_STARTPOINT)
    //debugPrintFAC()
    

    //+ width = endpoint
    LoadFAC(FAC_REAL_STARTPOINT)
    AddMemFAC(FAC_LINE_WIDTH)
//debugPrintFAC()    
    //but if Exp >= 88 then we know it's 128 or larger
    //if endpoint <0 or endpoint >256 it's always out of view (in latter case because width is max 128)
    lda $66
    bpl !+
    jmp ignoreAndNext
!:  lda $61
    cmp #$89
    bcc !+
    jmp ignoreAndNext    
!:  cmp #$88
    bcc storeEP
    LoadFAC(hundredTwentEight)
storeEP:    
    StoreFAC(FAC_ENDPOINT)
    

    //switch point = startpoint + switchfactor*width
    LoadFAC(FAC_SWITCH_FACTOR)
    MultMemFAC(FAC_LINE_WIDTH)
    AddMemFAC(FAC_REAL_STARTPOINT)

    //check if < 0
    lda $66
    bpl checkLeft
    LoadFAC(zero)
    jmp storeSWP
checkLeft:
    //but if Exp >= 88 then we know it's 128 or larger
    lda $61
    cmp #$88
    bcc storeSWP
    LoadFAC(hundredTwentEight)
storeSWP:
    StoreFAC(FAC_SWITCHPOINT)
    //debugPrintFAC()
toInt:
//no more sanity checks needed?
    LoadFAC(FAC_STARTPOINT)
    jsr $bc9b    //QINT
    lda $65
    ldy INT_Y
    sta START_POINTS,y

    LoadFAC(FAC_ENDPOINT)
    jsr $bc9b    //QINT
    lda $65
    ldy INT_Y
    sta END_POINTS,y

    LoadFAC(FAC_SWITCHPOINT)
    jsr $bc9b    //QINT
    lda $65
    ldy INT_Y
    sta SWITCH_POINTS,y

next:    
    LoadFAC(FAC_TMP)    
    AddMemFAC(one)
    StoreFAC(FAC_TMP)
    jmp loop
ignoreAndNext:
    lda #$ff
    ldy INT_Y
    sta START_POINTS,y
    jmp next

pointsDone:
    // check if switch point beyond end point
    ldx #$40
!:  lda SWITCH_POINTS,x
    cmp END_POINTS,x
    bcc isOk 
    lda END_POINTS,x
isOk:
    sta SWITCH_POINTS,x
    dex
    bpl !-
    //same for switch points smaller than start point
    ldx #$40
!:  lda SWITCH_POINTS,x
    cmp START_POINTS,x
    bcs isOk2 
    lda START_POINTS,x
isOk2:
    sta SWITCH_POINTS,x
    dex
    bpl !-

    //now mirror points
    ldx #$00
    ldy #$7f
!:  lda START_POINTS,x
    sta START_POINTS,y
    lda END_POINTS,x
    sta END_POINTS,y
    lda SWITCH_POINTS,x
    sta SWITCH_POINTS,y
    inx
    dey
    cpx #$40
    bne !-
    rts

yTemp:  .byte 1

//constant floating points

FAC_FOV_DIV_2: 
    .byte $80, $49, $0f,  $da, $a2
FAC_Y:  intToCompressedFloat(Y)
sixtyFourSquared:
    intToCompressedFloat(64*64)
ySquared:
    intToCompressedFloat(Y_SQUARED)
zero:   .byte 0,0,0,0,0 
//TODO see if can be exchanged for BASIC value, which is -100 at    BF2E 
one:
    intToCompressedFloat(1)

five:
    intToCompressedFloat(5)
//TODO see if can be exchanged for BASIC value, which is -100 at    BF2E 
hundred:
    intToCompressedFloat(100)

maxX: intToCompressedFloat(5000)    
//three values all the same, so same memory, three variables
planetMaxSize: // intToCompressedFloat(PLANET_SIZE)
hundredTwentEight:
screenWidth: intToCompressedFloat(128)  //screen size

sixtyFour:
screenWidthDiv2: intToCompressedFloat(64)//half screen size
oneMinusVDivC:
    .byte $80,$7f,$ea,$23,$b7
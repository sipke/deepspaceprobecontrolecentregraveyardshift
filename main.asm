.var SKIP_INTRO = (cmdLineVars.get("skipintro").asBoolean()==true)

.import source "basic_functions.asm"
.import source "macros.asm"
.import source "bitDraw.asm"
.import source "interrupt.asm"


       *= $0801 "Basic Upstart"
        BasicUpstart($080d)    
        *= $080d "main"
        copyStuff()
        generatePointers()
        createAngleTableM(zero)    
        createDecToPetsciiTableM()    
        createPetsciiAngleM()

      	lda #0
    	sta repeats
        sta state

        setupInterrupt(irq)
        lda #1
        sta doStateChange
        jmp entryPoint
.memblock "texts"
texts:
{
    .import source "texts.asm"
}

irq:
    irqM()
//calcSphere
    .import source "calcSphere.asm"

.memblock "drawdots"
calcAddressFromXY:
    calcAddressFromXYM()
drawLine:
    drawlineM()        
drawPlanet:
    drawPlanetM(drawLine, calcAddressFromXY)    
clearScreen:
    clearScreenM()    
    rts
getNextRand:    
    getNextRandM()    
    rts  
getRandomBelow:    
    getRandomBelowM()    
    rts  
setState:
	sta state
	sta doStateChange   //possible because all state values except STARTUP are non zero and non zero means doStateChange
    rts
.memblock "main loop"
mainLoop:
entryPoint:
    lda doStateChange
    beq noStateChange
    jmp performStateChange
    jmp mainLoop
noStateChange:
    lda state
    cmp #STATE_STARTUP
    bne checkWait
    jsr calculateCircleTable
    setStateM(STATE_WAIT_FOR_USER, setState)
    jmp mainLoop
checkWait:    
    cmp #STATE_WAIT_FOR_USER
    bne checkIntro1
    //íncrease random seed
    jsr getNextRand
    lda $DC01
    cmp #JOY1_FIRE
    bne !+
    initM()
    setStateM(STATE_INTRO_1, setState)
!:  jmp mainLoop
checkIntro1:
    cmp #STATE_SET_EVENT
    bne checkIntroDone
    lax eventType
    bpl writeText
    jmp handleCustomEvent
writeText:    
    jsr clearPreviousText
    lax eventParameter
    and #%00000111
    clc
    adc #TEXT_Y_OFFSET
    sta textAtY
    txa 
    lsr 
    lsr
    lsr
    clc
    adc #TEXT_X_OFFSET
    sta textAtX
    lda eventType
    ldx textAtX
    ldy textAtY
    jsr printTextAt
endStateChange:
    lda #0
    sta doStateChange
    lda #STATE_INTRO_1
    sta state
    jmp mainLoop
checkIntroDone:
    cmp #STATE_INGAME
    beq doInGame
    cmp #STATE_WAIT_FOR_LAST_DEVELOPMENT
    bne notInGame
doInGame:    
    ldx #0
    stx curPhotoHandling
photoLoop:
    ldx curPhotoHandling
    lda photoStatus,x
    cmp #PHOTO_STATE_TAKEN
    bne !+
    jsr developPhoto 
    ldx curPhotoHandling 
!:  inx
    cpx #MAX_NR_PHOTOS
    beq !+
    stx curPhotoHandling
    jmp photoLoop
!:
    //check if photo arriving event is passed if so add one. If all photos are received, goto view phase
    ldy #0  //nr of photos received
    ldx #0
photoReceiveLoop:
{
    
    lda photoEventReceivedAt+1,x
    beq next   //cheap way to determine if moment not passed. Because it takes more time to receive one photo than 1 tick of framecountHi
                        //means: if timearrive is zero, that photo arrive time is not calculated yet
//break()                        
    cmp frameCountHi
    bcs next   //mmoment not passed yet, so not all than, goto exit
//    lda photoEventReceivedAt,x
//    cmp frameCountLo  //only compare lower if higher is EQUAL dumbo!
    bcs next
    iny
next:    
    inx
    inx
    cpx #(2*MAX_NR_PHOTOS)
    bne photoReceiveLoop
    
    tya
    clc
    adc #$30
    sta receivedWritePos

    cpy #MAX_NR_PHOTOS
    bne notAllReceived
    setStateM(STATE_SHOWING_PHOTOS, setState)

notAllReceived:    
    jmp mainLoop
}

notInGame:
    cmp #STATE_SHOWING_PHOTOS
    bne !+
    lda viewingPhoto
    clc
    adc #$31
    sta lastText + 1
    lda #31
    ldx #38
    ldy #19
    jsr printTextAt
!:    
    jmp mainLoop
//----------------------- HANDLE CUSTOM EVENT
handleCustomEvent:
{
    cmp #FAKE_RUSSIAN_FONT_EVENT
    bne !+
    setCharsAt3800()
    jmp endStateChange
!:  cmp #STANDARD_FONT_EVENT
    bne !+
    jsr clearScreen
    setCharsAtDefault()
    jmp endStateChange
!:  cmp #SETUP_INTR_OPENING
    bne !+
    jmp setupOpeningScreen
!:  cmp #LOOK_UP_EVENT
    bne !+
    lda #PAVEL_LOOK_RIGHT
    sta 2042
    lda #0
    sta isAnimating
    jmp endStateChange
!:  cmp #SETUP_INTRO_2
    bne !+
    jmp setupOpeningScreen2
!:  cmp #SETUP_INTRO_3
    bne !+
    jmp setupOpeningScreen3
!:  cmp #SETUP_GAME_SCREEN
    bne !+
    jmp setupGameScreen
!:  jmp endStateChange
}
setupGameScreen:
{
    ldx #0
    stx $d015
!:
.for(var i=0; i<1000; i+=250) {
    lda screen_002+i+2,x
    sta $0400+i,x
    lda screen_002+1000+i+2,x
    sta $d800+i,x
}
    inx
    bne !-
    
    jsr randomizeDistancesAndSaveToFAC

    lda #STATE_INGAME
    sta state

    jmp endStateChange
}
setupOpeningScreen3:
{
    jsr clearScreen
    //draw line
    ldx #LINE_LENGTH
!:  lda #LINE_CHAR
    sta $0400+LINE_Y*40 + LINE_OFFSET -1,x
    lda #1
    sta $d800+LINE_Y*40 + LINE_OFFSET -1,x
    dex 
    bne !-

    //put sprites    
    ldx #11
!:  lda spriteXYPositionsScene3,x
    sta $d000,x
    //these attributes are only halve of nr positions, but never mind no danger in writing a few more bytes higher
    // 2040-2047 2048=$0801 till $080d is basic run, OK to overwrite that
    lda spriteDefsScene3,x
    sta 2040,x
    lda spriteColorsScene3,x    //$d030-d040 not connected
    sta $d027,x
    dex
    bpl !-

    lda #%00110011
    sta $d01C

    lda #%00111111
    sta $d015
    jmp endStateChange
}
setupOpeningScreen2:
{
    jsr clearScreen
    //draw line
    ldx #LINE_LENGTH
!:  lda #LINE_CHAR
    sta $0400+LINE_Y*40 + LINE_OFFSET -1,x
    lda #1
    sta $d800+LINE_Y*40 + LINE_OFFSET -1,x
    dex 
    bne !-
    
    //put sprites    
    ldx #11
!:  lda spriteXYPositionsScene2,x
    sta $d000,x
    //these attributes are only halve of nr positions, but never mind no danger in writing a few more bytes higher
    // 2040-2047 2048=$0801 till $080d is basic run, OK to overwrite that
    lda spriteDefsScene2,x
    sta 2040,x
    lda spriteColorsScene2,x    //$d030-d040 not connected
    sta $d027,x
    dex
    bpl !-

    lda #%00110011
    sta $d01C

    lda #%00111111
    sta $d015

    jmp endStateChange
}    
setupOpeningScreen:
{
    jsr clearScreen

    ldx #LINE_LENGTH
    //draw line
!:  lda #LINE_CHAR
    sta $0400+LINE_Y*40 + LINE_OFFSET -1,x
    lda #1
    sta $d800+LINE_Y*40 + LINE_OFFSET -1,x
    dex 
    bne !-
    //put sprites    
    ldx #7
!:  lda spriteXYPositionsScene1,x
    sta $d000,x
    //these attributes are only halve of nr positions, but never mind no danger in writing a few more bytes higher
    lda spriteDefsScene1,x
    sta 2040,x
    lda spriteColorsScene1,x
    sta $d027,x
    dex
    bpl !-

    lda #1
    sta isAnimating

    lda #MULTI_COLOR_1
    sta $d025
    lda #MULTI_COLOR_2
    sta $d026

    lda #%00001100
    sta $d01C

    lda #%00001111
    sta $d015

    lda #$ff
    sta $d01d
    sta $d017

    jmp endStateChange
}

developPhoto:
{

    ldx curPhotoHandling
    // copy photo attributes to working area
    lda photoAtAngleIndex,x 
    sta sm1 + 1
    asl //times two
    asl //times four
    clc
sm1:    
    adc #0  //times five
    tax
    ldy #0
!:  lda angleFACTable,x
    //sta photoAtAngleFACTemp,y
    sta FAC_ANGLE_RAD,y
    inx
    iny
    cpy #5
    bne !-
    lda curPhotoHandling    //photo index
    asl
    asl
    asl // x8
    clc
    adc #<photoAtDist
    sta $7a
    lda #>photoAtDist
    adc #0
    sta $7b

    jsr $79
    jsr STR_TO_FAC

    StoreFAC(FAC_X)

    jsr calculateView

    jsr drawPlanet
    
    //drawRandomDots()

//===========
    copyPhotoToMem()

    ldx curPhotoHandling
    lda #PHOTO_STATE_READY
    sta photoStatus,x

doneDevelop:
    rts
}

performStateChange:
    lda state
    cmp #STATE_STARTUP
    bne notStateStartup
    jsr clearScreen
    lda #0
    ldx #10
    ldy #10
    jsr printTextAt
    jmp end
notStateStartup:
    cmp #STATE_WAIT_FOR_USER
    bne notWaiting
    lda #$15
    sta $d018
    lda #$97
    sta $dd00

    jsr clearScreen
    lda #1
    ldx #10
    ldy #10
    jsr printTextAt

    jmp end
notWaiting:
    cmp #STATE_SHOWING_PHOTOS
    beq !+
    jmp notShowingPhotos
!:
    jsr clearScreen
    writeCharsRectM()    
    setVIC(1, $800, $0000)

    ldx #240
!:  lda #1
    sta $d800-1+19*40,x
    lda #$20
    sta $0400-1+19*40,x
    dex
    bne !-

    lda #22
    ldy #19
    ldx #0
    jsr printTextAt
    lda #23
    ldy #20
    ldx #0
    jsr printTextAt
    lda #24
    clc
    ldx score + 1
    cpx #scoreThreshold2
    bcc !+
    clc
    adc #2
!:   cpx #scoreThreshold1
    bcc !+
    clc
    adc #2
!:  tax
    inx
    stx text2 + 1
//score dependent texts
text1:
    ldy #21
    ldx #0
    jsr printTextAt
text2:    
    lda #25
    ldy #22
    ldx #0
    jsr printTextAt

    lda #30
    ldy #23
    ldx #0
    jsr printTextAt

    ldx score
    lda score + 1
    jsr $bdcd

    jsr drawPlanet
    

    jmp end
notShowingPhotos:

end:
    lda #0
    sta doStateChange
    jmp mainLoop

piDividedBy36:
	.byte $7d, $32, $b8, $c2, $57

.memblock "sprites"
sprites:
{
   .import binary "resources/sprites.bin"

}
randomSeed: 
    .byte $1b

.memblock "anim data"
.import source "spriteAnimData.asm"
.memblock "screen data"
.import source "./resources/screen/screen.petmate.asm"

.memblock "ingame logic"
.import source "ingame_logic.asm"

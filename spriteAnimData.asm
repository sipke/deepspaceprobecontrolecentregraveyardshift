#importonce

.import source "constants.asm"
//========= sprite anim data ================

//scene 1: pavel sweeping, broom sprite 0,1 in front of Pavel, sprite 2,3

//scene 2: pavel kneeling behind professor, broom in front of pavel, professor sprite 0,1, broom sprite 2,3 Pavel sprite 4,5
//        computer in background sprite 6,7

//scene 3: pavel staring at computer holding broom, pavel sprite 0,1 broom sprite 2,3, computer sprite 4,5

spriteXYPositionsScene1:
    .byte 100,140,100,182,100,140,100,182
spriteXYPositionsScene2:
    .byte 102,172,150,172,100,150,100,192,100,150,100,192
spriteXYPositionsScene3:
    .byte 131,146,131,188,162,146,162,188, 110,130, 110,172
spriteDefsScene1:
    .byte BROOM_SWEEP_0_TOP, BROOM_SWEEP_0_BOTTOM, PAVEL_SWEEP_0_TOP, PAVEL_SWEEP_0_BOTTOM
spriteDefsScene2:
    .byte PROFESSOR_LEFT, PROFESSOR_RIGHT, BROOM_STRAIGHT_TOP, BROOM_STRAIGHT_BOTTOM, PAVEL_KNEEL_TOP, PAVEL_KNEEL_BOTTOM
spriteDefsScene3:
    .byte PAVEL_BACK_STARE_TOP, PAVEL_BACK_STARE_BOTTOM , BROOM_STRAIGHT_TOP, BROOM_STRAIGHT_BOTTOM, COMPUTER_TOP, COMPUTER_BOTTOM
spriteColorsScene1:
    .byte BROOM_COLOR, BROOM_COLOR, PAVEL_COLOR, PAVEL_COLOR
spriteColorsScene2:
    .byte PROFESSOR_COLOR, PROFESSOR_COLOR, BROOM_COLOR, BROOM_COLOR, PAVEL_COLOR, PAVEL_COLOR 
spriteColorsScene3:
    .byte PAVEL_COLOR, PAVEL_COLOR , BROOM_COLOR, BROOM_COLOR, COMPUTER_COLOR, COMPUTER_COLOR

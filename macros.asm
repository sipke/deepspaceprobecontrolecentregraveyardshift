#importonce

.import source "constants.asm"
.import source "basic_functions.asm"

//https://www.codebase64.org/doku.php?id=base:8bit_atan2_8-bit_angle
//https://www.codebase64.org/doku.php?id=base:mandelbrot

.var brkFile = createFile("debug.txt") 
.macro break() {
    .eval brkFile.writeln("break " + toHexString(*))
}

.const LDFAC = $bba2
.const STFAC = $bbd4
.const SUBFAC = $b850
.const DIVFAC = $bb0f
.const CMPFAC = $bc5b
.const MULFAC = $ba28 
.const ADDFAC = $b867
.const SETFAC = $b391

.const LDFAC2 = $ba8c
.const CPFAC1FAC2 = $bc0f
.const CPFAC2FAC1 = $bbfc
.const DIVFAC2BYFAC1 = $bb12
.const MULFAC2BYFAC1 = $ba2b //ba33?

.macro intToCompressedFloat(n) {
    //.print "n = " + n
    .var b0
    .var b1
    .var b2
    .var b3
    .var b4
	.var e=log(abs(n))/log(2)
    .eval b0 = $80 + (e+1)
    //.print "abs n=" + n + "," + toBinaryString(abs(n))
    .var s=toBinaryString(abs(n))
	//negate first digit, that is implied in the exponent
	.eval s = s.substring(1,s.size())
	//sign is first bit
    .if (n<0) {
        .eval s="1" + s
    }else {
        .eval s="0" +s
    }
	//expand to 32 bits
    .var to= 32-s.size()
    .for (var j=0;j<to; j++){
        .eval s= s+ "0"
    }
    //.print s
    // .if(n<0) { 
    //     .eval s="1"+s
    // }else {
    //     .eval s="0"+s
    // }
	//.print s
    .eval b1 = s.substring(0,8).asNumber(2)
    .eval b2 = s.substring(8,16).asNumber(2)
    .eval b3 = s.substring(16,24).asNumber(2)
    .eval b4 = s.substring(24,32).asNumber(2)
    // .print toHexString(b0)
    // .print toHexString(b1)
    // .print toHexString(b2)
    // .print toHexString(b3)
    // .print toHexString(b4)
	.byte b0,b1,b2,b3,b4
}

.macro intToFACFloat(n) {
    //.print "n = " + n
    .var b0
    .var b1
    .var b2
    .var b3
    .var b4
	.var b5
    .var e=log(n)/log(2)
    .eval b0 = $80 + (e+1)

    //.print toBinaryString(n)
    .var s=toBinaryString(n)

    .if (n<0) {
        .eval b5=$ff
    }else {
        .eval b5=$00
    }

    .var to= 32-s.size()
    .for (var j=0;j<to; j++){
        .eval s= s+ "0"
    }
    //.print s
    .eval b1 = s.substring(0,8).asNumber(2)
    .eval b2 = s.substring(8,16).asNumber(2)
    .eval b3 = s.substring(16,24).asNumber(2)
    .eval b4 = s.substring(24,32).asNumber(2)
    //.print toHexString(b0)
    //.print toHexString(b1)
    //.print toHexString(b2)
    //.print toHexString(b3)
    //.print toHexString(b4)
	//.print toHexString(b5)
	.byte b0,b1,b2,b3,b4,b5
}

//10000 = $2710 
//E = 14 ? codebase suggests E = E-$80, but it's actually one more
//remainder = 1808 = $710 = %011100010000 => %01110001..00 leading zero for positive => %00111000 10000000 ..0 = $38 $80 ?
//but stored as float registers is different
//E=14	=> $8E
// $2710 => %100111000100..00 => %1001 1100 %0100 0000 => $9c $40 $00 $00 (sign : $00)

.macro SQUAREFAC1() {
	jsr CPFAC1FAC2
	jsr MULFAC2BYFAC1
}

.macro LoadFACInt(number) {
    lda #>number
    ldy #<number
    jsr FUNC_INT_TO_FAC1
}

.macro LoadFAC(addr) {
	lda #<addr
	ldy #>addr
	jsr LDFAC
}

.macro LoadFAC2(addr) {
	lda #<addr
	ldy #>addr
	jsr LDFAC
}

.macro StoreFAC(addr)  {
	ldx #<addr
	ldy #>addr
	jsr STFAC
}
//subtract FAC from mem
.macro SubFACFromMem(addr){
	lda #<addr
	ldy #>addr
	jsr SUBFAC
}
//subtract mem from FAC
.macro SubMemFromFAC (addr) {
	StoreFAC(FAC_WORK_AREA)
	LoadFAC(addr)
	SubFACFromMem(FAC_WORK_AREA)
}

//divide value in memory by FAC
.macro DivMemByFAC (addr) {
	lda #<addr
	ldy #>addr
	jsr DIVFAC
}
//divide FAC by value in memory
.macro DivFACByMem (addr) {
	StoreFAC(FAC_WORK_AREA)
	LoadFAC(addr)
	DivMemByFAC(FAC_WORK_AREA)
}
//compare fac with mem, A = 1 if mem > fac, A=0 if fac=mem, A=-1 if fac>mem
.macro CmpFACMem (addr) {
	StoreFAC(FAC_WORK_AREA)
	LoadFAC(addr)
	CmpMemFAC(FAC_WORK_AREA)
	pha
//TODO: problematic, you lose result
	LoadFAC(FAC_WORK_AREA)
	pla
}
//compare  mem with fac, A = 1 if fac>mem, A=0 if fac=mem, A=-1 if mem > fac
.macro CmpMemFAC (addr) {
	lda #<addr
	ldy #>addr
	jsr CMPFAC
}

.macro MultMemFAC (addr) {
	lda #<addr
	ldy #>addr
	jsr MULFAC
}

.macro AddMemFAC(addr) {
	lda #<addr
	ldy #>addr
	jsr ADDFAC
}
//divide FAC by 2^number (which is exp2-number), 
//you can use negative numbers
.macro DivFACExp2(number) {
	lda $61
	beq !+	//zero stays zero
	sec
	sbc #number 
	sta $61	
!:
}
//mulitply FAC by 2^number (which is exp2-number), 
//you could use negative numbers
.macro MulFACExp2(number) {
	lda $61
	beq !+	//zero stays zero
	clc
	adc #number 
	sta $61	
!:	
}
//divide Float in memory by 2^number (which is exp2-number), 
//you can use negative numbers
.macro DivMemExp2(mem, number) {
	lda mem
	sec
	sbc #number 
	sta mem	
}
//multiply Float in memory by 2^number (which is exp2-number), 
//you could use negative numbers
.macro MulMemExp2(mem,number) {
	lda mem
	clc
	adc #number 
	sta mem	
}
//.macro fasterSQRT(valueLocation, resultLocation, FAC2VARFUNCTION, VAR2FACFUNCTION, DIVFUNCTION, ADDFUNCTION) {
.macro fasterSQRT(valueLocation, resultLocation) {
	.const FAC2VARFUNCTION = STFAC
	.const VAR2FACFUNCTION = LDFAC
	.const DIVFUNCTION = DIVFAC
	.const ADDFUNCTION = ADDFAC
sqrt:	
    ldx #<valueLocation
	ldy #>valueLocation
	jsr FAC2VARFUNCTION
	//lda valueLocation+1
//	bmi error	//no negative square root
	lda valueLocation
	beq done	//root of 0 is 0
 
	ldy #$00	//fill temp result with 0
	sty resultLocation+1
	sty resultLocation+2
	sty resultLocation+3
	sty resultLocation+4
 
	lda valueLocation	//get guess based on argument
        clc
	ror
	bcs sqrtadd
	ldx #$80
	stx resultLocation+1
sqrtadd:
     adc #$40
	sta resultLocation
	lda valueLocation+1
	ora resultLocation+1
	lsr
	lsr
	lsr
	lsr
	tax
	lda sqrttable,x
	sta resultLocation+1
	lda #04		//4 iterations of newton's method
	sta $fb
	lda #<resultLocation
	ldy #>resultLocation
	jsr VAR2FACFUNCTION
!:	lda #<valueLocation
	ldy #>valueLocation
	jsr DIVFUNCTION
	lda #<resultLocation
	ldy #>resultLocation
	jsr ADDFUNCTION
	dec $61
	ldx #<resultLocation
	ldy #>resultLocation
	jsr FAC2VARFUNCTION
	dec $fb
	bne !-
done:	rts
 
//error:	rts
 
sqrttable:
	.byte 03,11,18,25,32,38,44,50
	.byte 58,69,79,89,98,107,115,123
}

.macro debugPrint(fac){
    LoadFAC(fac)
    jsr FAC1_TO_STRING
    jsr PRINT_STRING
}
.macro debugPrintFAC(){
    jsr FAC1_TO_STRING
    jsr PRINT_STRING
}
.macro copyStuff() {
	sei
	copyCharsToRAM()
	reverseRAndN()
	copySprites()
	cli
}

.macro initM() {
	ldy #$ff
	sty currentEventIndex
	ldy #1
	// 1
	sty eventDurationLo
	sty eventDurationHi
	lax #0
!:	sta BASE_ZERO_CONSTANTS_START,x
	inx
	bne !-
}
// A should contain value where result should stay below 
.macro getRandomBelowM(){
	sta sm1 + 1
again:	
	lda randomSeed
    beq doEor
    clc
    asl
    beq noEor    //if the input was $80, skip the EOR
    bcc noEor
doEor:     eor #$1d
noEor:     sta randomSeed

sm1: cmp #0
	bcs again
}
.macro getNextRandM(){
	lda randomSeed
    beq doEor
    clc
    asl
    beq noEor    //if the input was $80, skip the EOR
    bcc noEor
doEor:     eor #$1d
noEor:     sta randomSeed
}
.macro copySprites(){
	ldy #5
outer:	
	ldx #0
inner:
	lda sprites,x
	sta SPRITE_MEM_BASE,x
	inx
	bne inner
	inc inner+2
	inc inner+5
	dey
	bne outer
}
.macro copyCharsToRAM(){
	lda #$33
	sta $1
	ldx #8
out:
	ldy #0
in:	lda $d000,y
	sta CHARS_AT,y
	iny
	bne in
	inc in + 2
	inc in + 5
	dex
	bne out
	lda #$37
	sta $1
}
.macro reverseRAndN(){
	ldx #7
!:	lda CHARS_AT + 14*8,x
	sta temp16bytes,x
	lda CHARS_AT + 18*8,x
	sta temp16bytes+8,x
	dex
	bpl !-
	//reverse them
	ldx #7
nl:	ldy #8
!:	asl temp16bytes,x
	ror CHARS_AT + 14*8,x
	asl temp16bytes+8,x
	ror CHARS_AT + 18*8,x
	dey
	bne !-
	dex
	bpl nl
}
.macro setCharsAt3800(){	
	lda	#$1e
	sta	$d018
}
.macro setCharsAtDefault(){	
	lda	#$15
	sta	$d018
}

.macro createAngleTableM(zeroLoc){
	lda #0
	sta repeats
	LoadFAC(zeroLoc)

rep:
	ldx #<angleFACTable
	ldy #>angleFACTable
	jsr STFAC
	lda rep+1
	clc
	adc #5
	sta rep+1
	bcc !+
	inc rep+3
!:	ldy repeats
	lda rep +1
	sta angleTablePointerLo,y
	lda rep +3
	sta angleTablePointerHi,y
	iny
	sty repeats
	cpy #37
	beq done
	SubMemFromFAC(piDividedBy36)	
	jmp rep
done:
}

.macro createPetsciiAngleM() {

	lax #0
	tay
v1:	lda #$30
	sta intAnglePetsciiTable,y
v2:	lda #$30
	sta intAnglePetsciiTable+2,y
	lda decToPetsciiTableLo,x
	sta intAnglePetsciiTable + 1,y
	tya
	and #$01
	beq notOdd
	inx
	cpx #10
	bne notOdd
	ldx #0
notOdd:	
	lda v2 +1
	eor #$05
	sta v2 +1
	iny
	iny
	iny
	cpy #60
	bne !+
	inc v1+1
!:	cpy #3*37
	bne v1	

}


.macro createDecToPetsciiTableM() {
	ldy #0
!:	tya
	and #$f
	clc
	adc #$30
	sta decToPetsciiTableLo,y
	tya
	lsr 
	lsr
	lsr
	lsr
	clc
	adc #$30
	sta decToPetsciiTableHi,y
	iny
	bne !-
}

.macro setVIC(bank, charDot, videoMatrix) {
	.print "chardot=" + toBinaryString(charDot/1024)
	.print "videoMatrix=" + toBinaryString(((videoMatrix/1024)<<4))
	.print "results in " + toBinaryString((charDot/1024) | ((videoMatrix/1024)<<4))
	lda #(charDot/1024) | ((videoMatrix/1024)<<4)
	sta $d018
	.print "bank value " + toBinaryString($97 & %11111100 | (3-bank))
	lda #$97 & %11111100 | (3-bank)
	sta $dd00
}


.macro copyPhotoToMem() {
    lda curPhotoHandling    //photo index
	asl 
	asl
	asl
	adc #>PHOTO_CHARS_BASE
	sta sm1 +2
	lda #>PHOTO_WORK_AREA
	sta sm2 + 2
	ldy #8
	ldx #0
sm2: lda PHOTO_WORK_AREA,x
sm1: sta PHOTO_CHARS_BASE,x
	inx
	bne sm2
	inc sm1 + 2
	inc sm2 +2
	dey
	bne sm2

}
.macro setStateM(value, sr){
	lda #value
	jsr sr
}

.macro drawRandomDots() {
//NOICE TO HAVE draw some random dots

	ldx #$40 
!:  jsr getNextRand
	sta smx+1
	sta smy+1
	and #$07
	clc
	adc #>PHOTO_WORK_AREA
	sta smx+2
	sta smy+2
	lda #%01000000
smx: ora PHOTO_WORK_AREA
smy: sta PHOTO_WORK_AREA       
	dex
	bne !-

}
#importonce

.import source "basic_functions.asm"
.import source "macros.asm"
.import source "constants.asm"



randomizeDistancesAndSaveToFAC:


// player needs to have time to do calculations, let's say approx 60 seconds?
// we want large enough variation in starting distance so player cannot use same timing every play
// if we use variation of 0-9 million km -> 0-30 seconds difference is 0-3000 km, which is fine
// if we start at 20 million km is ~ 70 seconds for signal to travel, at 10 mil ~ 33 sec
// so starting distance probe to planet must be large enough so you won't have passed it yet, but close enought to miss it
// if we assume max dist of 19 mil to earth, delay of 63 seconds. After 63 seconds +  calculation time the probe should still be 
// at a reasonable distance to planet 
// take a 60 second calculation time + min 63 second delay, times 100km/s probe speed means a minimal distance ~12000 km from planet
// we'll be a bit less forgiving and make it 10000 + random factor 10000 - 14000 range

    // lda #4 //variaTion 0-3
    // jsr getRandomBelow
    // and #$00
    // clc
    // adc #$f3//+ 6 as staring value, f means negative
    // sta distProbeToPlanetDecimal
    // and #$0f
    // adc #$30
    // sta planetDistWritePos + 1
    
    //init values
    lda #$2d//minus
    sta planetDistWritePos
    ldx #$32
    stx planetDistWritePos + 1
    dex
    stx earthDistWritePos
    lda #$82
    sta distProbeToPlanetDecimal
    lda #$10
    sta distProbeToEarthDecimal

break()    
    lda #4
    jsr getRandomBelow
    clc
    adc #$30
    sta planetDistWritePos + 2
    and #$0f
    asl
    asl
    asl
    asl
    clc
    adc distProbeToPlanetDecimal + 1
    sta distProbeToPlanetDecimal +1



    lda #$0a
    jsr getRandomBelow
    clc
    adc distProbeToEarthDecimal
    sta distProbeToEarthDecimal
    //asume less then 10
    and #$0f
    adc #$30
    sta earthDistWritePos + 1


    lda #<planetDistWritePos
    sta $7a
    lda #>planetDistWritePos
    sta $7b

    jsr $79
    jsr STR_TO_FAC

    StoreFAC(distProbeToPlanetFac)


    lda #<earthDistWritePos
    sta $7a
    lda #>earthDistWritePos
    sta $7b

    jsr $79
    jsr STR_TO_FAC

    StoreFAC(distProbeToEarthFac)

    AddMemFAC(distProbeToPlanetFac)

    StoreFAC(distanceEarthToPlanet)

    rts

distProbeToPlanetDecimal:
    .byte $81,$05,$99
distProbeToEarthDecimal:
    .byte $10,$59,$79,$99
    
lightSpeedFAC:
    intToCompressedFloat(LIGHT_SPEED)

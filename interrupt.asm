#importonce
.import source "macros.asm"
.import source "constants.asm"
//.import source "texts.asm"

//------------------------ INTERRUPT CODE -----------------
.macro setupInterrupt(irq){
    sei
    lda #<irq
    sta $0314
    lda #>irq
    sta $0315
    lda #$1b
    sta $d011
    lda #IRQ_LINE
    sta $d012
    lda #$01
    sta $d01a
    cli
}

.macro irqM(){
    lsr $d019
    bcs !+
    jmp $ea31
!:
    //inc $d020

    lda state
    cmp #STATE_INTRO_1
    beq !+ 
    jmp stateCheck2
    //if duration = 0 -> do next event
!:  lda eventDurationLo
    bne noStateChange
    lda eventDurationHi
    bne noStateChange
//--------------- new event -------------
    inc currentEventIndex
    ldx currentEventIndex
    cpx #MAX_EVENTS
    .print "max evenst" + MAX_EVENTS
    bne !+
    lda #STATE_INGAME
    sta state
    jmp end
    //trigger state change code outside interrupt
!:  setStateM(STATE_SET_EVENT, setState)
    //read and write event bytes for state changer
    lda events,x
    sta eventType
    lda #0
    sta eventDurationHi
    lda eventDurations,x
    clc
    asl
    lsr eventDurationHi
    asl
    lsr eventDurationHi
    asl
    lsr eventDurationHi
    sta eventDurationLo
    inc eventDurationHi //wonky code but it works
    lda eventParameters,x
    sta eventParameter
    jmp end
noStateChange:
    dec eventDurationLo
    bne !+
    dec eventDurationHi
!:  lda isAnimating
    beq notAnimating
    ldx animationCounter
    inx
    cpx #ANIMATION_FRAME_COUNT
    bne noAnimate
    ldy #3
!:  lda 2040,y
    eor #2
    sta 2040,y
    dey
    bpl !-
    ldx #0
noAnimate:
    stx animationCounter
notAnimating:
    jmp end
stateCheck2:
    //in game: joystick, left/right update angle
    //fire: write distance , angle to queue
    //  update queue, max length then change state
    //wait for last photo arriving
    cmp #STATE_INGAME
    beq doJoy
    cmp #STATE_WAIT_FOR_LAST_DEVELOPMENT
    beq !+
    jmp stateCheck3
!:  jmp doneJoyCheck
doJoy:
    lda $DC00
    cmp prevJoyValue
    bne !+
    sta prevJoyValue
    jmp doneJoyCheck
!:  sta prevJoyValue
    cmp #JOY2_LEFT
    bne checkRight
    ldx currentAngleIndex
    bne !+
    jmp doneJoyCheck
!:  dex 
    jmp storeAndPrintAngle
checkRight:    
    cmp #JOY2_RIGHT
    bne checkFire
    ldx currentAngleIndex
    cpx #36
    bne !+
    jmp doneJoyCheck
!:  inx 
storeAndPrintAngle:
    stx currentAngleIndex
    stx adx+1
    txa
    asl 
adx:
    adc #0
    tax
    lda intAnglePetsciiTable+2,x
    sta angleWritePos + 3
    lda intAnglePetsciiTable+1,x
    sta angleWritePos + 2
    lda intAnglePetsciiTable,x
    sta angleWritePos + 1
//    lda #$20    //space   always positive
//!:  
//    sta angleWritePos
    jmp doneJoyCheck
checkFire:    
    cmp #JOY2_FIRE
    bne doneJoyCheck

//check if last photo is taken
    lda lastPhotoTaken
    asl 
    tax
    lda frameCountLo
    sta photoInStructionSentAtTime,x
    lda frameCountHi
    sta photoInStructionSentAtTime + 1,x

    ldx lastPhotoTaken
//    inx
    lda currentAngleIndex
    sta photoAtAngleIndex,x
    lda #PHOTO_STATE_TAKEN
    sta photoStatus,x
    txa
    asl 
    asl
    asl
    tax //distance index is index times 8

    ldy #0  //write chars of distance to planet including a closing char
!:  lda planetDistWritePos,y
    sta photoAtDist,x
    inx
    iny
    cpy #7
    bne !-
    //now add closing char, not +, -, E or point and not space
    lda #1  //A will suffice
    sta photoAtDist,x

//if last photo change state
    ldx lastPhotoTaken
    inx
    stx lastPhotoTaken
    txa
    clc
    adc #$30
    sta sentWritePos
    cpx #MAX_NR_PHOTOS
    bne doneJoyCheck
    setStateM(STATE_WAIT_FOR_LAST_DEVELOPMENT, setState)

doneJoyCheck:
    //update and print distances
    sed
    lda distProbeToEarthDecimal+3
    clc
    adc #PROBE_SPEED_PER_FRAME
    sta distProbeToEarthDecimal+3

    ldx #2
!:  lda distProbeToEarthDecimal,x
    adc #0
    sta distProbeToEarthDecimal,x
    dex
    bpl !-

    lda distProbeToPlanetDecimal
    bpl isPositive
    //decimal mode can't handle negative values, make sure it gets negative in the end
    //value is stored as positive  number, so speed must be SUBTRACTED
    lda distProbeToPlanetDecimal+2
    sec
    sbc #PROBE_SPEED_PER_FRAME
    sta distProbeToPlanetDecimal+2

    lda distProbeToPlanetDecimal + 1
    sbc #0
    sta distProbeToPlanetDecimal+1
    lda distProbeToPlanetDecimal
    and #%00001111  //ignore negative bit
    sbc #0
    sta distProbeToPlanetDecimal
    bcs noNullify
//if carry is cleared we've gone 'through' zero, but we don't want to get all nines, reset all to zero plus speed per frame
    lda #0
    sta distProbeToPlanetDecimal+1
    sta distProbeToPlanetDecimal
    clc
    adc #PROBE_SPEED_PER_FRAME
    sta distProbeToPlanetDecimal+2
    jmp endUpdateDist
noNullify:
    //keep negative
    lda distProbeToPlanetDecimal
    ora #$80
    sta distProbeToPlanetDecimal
    jmp endUpdateDist
isPositive:
    lda distProbeToPlanetDecimal+2
    clc
    adc #PROBE_SPEED_PER_FRAME
    sta distProbeToPlanetDecimal+2

    ldx #1
!:  lda distProbeToPlanetDecimal,x
    adc #0
    sta distProbeToPlanetDecimal,x
    dex
    bpl !-
endUpdateDist:
    cld
    //alternate printing dist to earth/planet
    lda frameCountLo
    and #$01
    beq printDistToEarth
    jmp doPrintDistToPlanet
printDistToEarth:
    //ldy #$20    //space
    //lax distProbeToEarthDecimal
    //bpl !+
    //ldy #45 //minus sign
    ldx distProbeToEarthDecimal
    lda decToPetsciiTableHi,x
    sta earthDistWritePos

    lda decToPetsciiTableLo,x
    sta earthDistWritePos +1

    ldx distProbeToEarthDecimal +1
    lda decToPetsciiTableHi,x
    sta earthDistWritePos +2
    lda decToPetsciiTableLo,x
    sta earthDistWritePos +3

    ldx distProbeToEarthDecimal +2
    lda decToPetsciiTableHi,x
    sta earthDistWritePos +4
    lda decToPetsciiTableLo,x
    sta earthDistWritePos +5

    ldx distProbeToEarthDecimal +3
    lda decToPetsciiTableHi,x
    sta earthDistWritePos +6
    lda decToPetsciiTableLo,x
    sta earthDistWritePos +7

    jmp distsPrinted
doPrintDistToPlanet:

    ldy #$20    //space
    lax distProbeToPlanetDecimal
    bpl !+
    ldy #45 //minus sign
!:  sty planetDistWritePos
    lda decToPetsciiTableLo,x
    sta planetDistWritePos +1

    ldx distProbeToPlanetDecimal +1
    lda decToPetsciiTableHi,x
    sta planetDistWritePos +2
    lda decToPetsciiTableLo,x
    sta planetDistWritePos +3

    ldx distProbeToPlanetDecimal +2
    lda decToPetsciiTableHi,x
    sta planetDistWritePos +4
    lda decToPetsciiTableLo,x
    sta planetDistWritePos +5


distsPrinted:
    inc frameCountLo
    bne !+
    inc frameCountHi
!:  


    jmp end
stateCheck3:
    cmp #STATE_SHOWING_PHOTOS
    beq !+
    jmp stateCheck4
!:    
{
    lda $DC00
    cmp prevJoyValue
    bne !+
    sta prevJoyValue
    jmp doneJoyCheck
!:    
    sta prevJoyValue
    cmp #JOY2_LEFT
    bne checkRight
    lda $D018
    cmp #D018_MIN_VALUE
    beq doneJoyCheck
    dec viewingPhoto
    sec
    sbc #2
    sta $D018
    jmp line2
checkRight:    
    cmp #JOY2_RIGHT
    bne checkFire
    lda $D018
    cmp #D018_MAX_VALUE
    beq doneJoyCheck
    inc viewingPhoto
    clc
    adc #2
    sta $D018
    cmp photosViewed    //save highest value to determine if all photos have been viewed
    bcc !+
    sta photosViewed
!:  jmp line2
checkFire:
    cmp #JOY2_FIRE
    bne doneJoyCheck
    lda photosViewed    //only react to fire after viewing all 6 photos
    cmp #D018_MAX_VALUE
    bne doneJoyCheck
    setStateM(STATE_WAIT_FOR_USER, setState)
//TODO    
doneJoyCheck:    
line2:
    lda #IRQ_LINE2
!:  cmp $d012
    bne !-
//delay some cycles so change is in blank part of screen
    ldx #8
!:  dex
    bne !-    
    //setVIC(0, $400, $1000)
    lda $d018
    sta smd018+1
    lda #$15
    sta $d018
    lda #$97
    sta $dd00


    lda #$ff//IRQ_LINE2 +40
!:  cmp $d012
    bne !-
    
smd018:    
    lda #0
	sta $d018
	lda #$97 & %11111100 | (3-1)
	sta $dd00

    jmp end
}    
stateCheck4:
 end:
    //dec $d020
    jmp $ea81
}




// state start
//      do nothing

//during intro: 
//      update timer

//during game: 
//  check joystick 
//      left / right : update angle
//      fire: write distance to earth and planet to index, increase index
//  update distance to earth and to planet in decimal mode and update screen

//  checking if time elapsed equal to message event
//      print messages 'sending instruction' 'receiving photo' 'out of range'
//  if max index reached and last photo received, end game


#importonce

// BASIC functions
//convert int to FAC1
.const FUNC_INT_TO_FAC1 = $b391

//Evaulate sign of FAC1 (returned in A) $bc39 = Evaulate sign of FAC1 (returned in FAC1)
//These routines return $01 for positive numbers, $ff for negative numbers, and 0 for zeros. If you know a number is non-zero, it is quicker just to use BIT $66, or LDA $66, etc.
.const FUNC_SIGN = $bc2b 

// Returns absolute value of FAC1, Simply LSR's the sign byte of FAC1 ($66) to put a zero in the MSB. This is easily done without the need for a JSR.
.const FUNC_ABS =$bc58 

//Round FAC1 to Integer
//Takes FAC1 and rounds away the decimal to make it an integer. It is still, however, expressed as a FP number.
//62, 63 contain 16 bit integer resp. HI LO
.const FUNC_INT = $bccc 

//Compare FAC1 with memory contents at A/Y (lo/high)
// A=1 IF ARG .LT. FAC.    
// A=0 IF ARG=FAC.
// A=-1 IF ARG .GT. FAC.
.const FUNC_COMP_FAC1_MEM =$bc5b

//Addition with memory contents pointed to by A/Y (low/high).
.const FUNC_ADD_FAC1_MEM=$b867

//Addition FAC2 The accumulator must load FAC1 exponent ($61) immediately before calling to properly set the zero flag.
//Add FAC1 with FAC2, leaving the result in FAC1. Load FAC2 after FAC1, or else mimic the Kernal's sign comparison beforehand.
.const FUNC_ADD_FAC1_FAC2=$b86a
 
//Subtraction with memory contents pointed to by A/Y (low/high).
.const FUNC_SUBT_FAC1_MEM=$b850

//Subtract FAC1 from FAC2 leaving result in FAC1. Negates FAC1 and falls through to the addition routine.
.const FUNC_SUBT_FAC1_FAC2=$b853
 
// Multiplication with memory contents pointed to by A/Y (low/high).
.const FUNC_MULT_FAC1_MEM=$ba28

//Multiply FAC1 with FAC2 leaving result in FAC1. Load FAC2 after FAC1, or else mimic the Kernal's sign comparison, then enter with the FAC1 exponent in A.
//Entry if FAC2 already loaded. Accumulator must load FAC1 exponent ($61) beforehand to set the zero flag.
.const FUNC_MULT_FAC1_FAC2=$ba2b


// Divide the memory contents pointed to by A/Y (low/high) by FAC1.
.const FUNC_DIV_FAC1_MEM=$bb0f
// Entry if FAC2 already loaded. Accumulator must load FAC1 exponent ($61) beforehand to set the zero flag. Sign comparison is not performed and ARISGN byte at $6f is not set, which has to be accounted for when using this entry point. Hard to debug sign errors may occur otherwise.
//Divides FAC2 by FAC1, leaving the quotient in FAC1, and the remainder in FAC2.
.const FUNC_DIV_FAC1_FAC2=$bb12

.const FAC1_TO_STRING = $bddd

//string to fac, lo in $7a, hi in $7b
.const STR_TO_FAC = $BCF3


.const FUNC_SIN_FAC1 =$e26b
.const FUNC_COS_FAC1 =$e264
.const FUNC_TAN_FAC1 =$e2b4
.const FUNC_ATAN_FAC1 =$e30e

.const FUNC_SQRT = $bf71

//converts floating point value in FAC to integer value to $62-$65 most significant byte first
.const FAC_TO_INT = $BC9B


//print null terminated string at address Y, A
.const PRINT_STRING = $ab1e

//set print position at x,y
.const SET_ROW_COL = $e50c

//srtandard CHROUT
.const FUNC_CHROUT = $ffd2
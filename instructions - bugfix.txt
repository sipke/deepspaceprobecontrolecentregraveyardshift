Hi,

Thanks for trying my game '1991 USSR deep space probe control centre janitor'. 

-----------------------------------------------
Notice: 

This is the bugfixed version. It's meant for people that can't run the original version.

Some people found that the game gets stuck after 'unpacking': A black 'A' is shown in the bottom right corner and nothing is happening. 
It doesn't affect everyone and has to do with differences in memory values at start up (thanks to Carleton Handley for the detective work!).

The difference between this version and the original is that this on sets one memory location to zero. 

Sorry for the inconvenience. Bad programming.
-----------------------------------------------------

Controls: use joystick in port 2
LEFT: to decrease the angle
RIGHT: to increse the angle
FIRE: to take a picture

For the real 'Pavel' experience I suggest you play with a pen/paper or a calculator and the PROBOS 2000 manual at hand, nothing else.


Credits:

I wrote all texts, code, made all graphics. 

But I couldn't have made it without:
 - the book 'Mapping the Commodore 64' by Sheldon Leemon 
 - codebase64.org for floating point calculations and probably for drawing on a 16x16 char rect and drawing lines on it (not sure where I got that)
 - online c64 Kernal and BASIC disassembly https://www.pagetable.com/c64ref/c64disasm/
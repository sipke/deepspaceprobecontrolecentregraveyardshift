!cpu 6510

;--------------------------
;SETUP
;
;--------------------------

create_masks
         ;generate mask tables
         ldx #$00
         txa
-
         sta maskr,x
         eor #$ff
         sta e_patt_left+$380,x
         sta e_patt_left+$180,x
         eor #$ff
         sec
         ror
         cmp #$ff
         bne +
         lda #$00
+
         inx
         bpl -

         ldx #$00
-
         lda e_patt_left+$180,x
         and e_patt+0
         sta e_patt_left+$000,x
         lda e_patt_left+$180,x
         and e_patt+1
         sta e_patt_left+$080,x
         lda e_patt_left+$180,x
         and e_patt+2
         sta e_patt_left+$100,x
         lda e_patt_left+$180,x
         and e_patt+4
         sta e_patt_left+$200,x
         lda e_patt_left+$180,x
         and e_patt+5
         sta e_patt_left+$280,x
         lda e_patt_left+$180,x
         and e_patt+6
         sta e_patt_left+$300,x
         inx
         bpl -
         rts

;--------------------------
;DRAWFACE
;fill face with 3/4 vertices with pattern
;--------------------------

;XXX TODO, if (y_max - y_min) * 2 < free space in zeropage -> render all to zeropage
;line1, render to $68++ offset 0, line2: dy < $100-line1end: render to zeropage as well, else render to $00xx,y
;set up start/end on tfXdrawline and in inner loop
;but implies to set up all targets in speedcode and linerendering beforehand on each run (lots of STAs?)
drawface
         ;is vertice 0 and 3 the same? (triangle)
         ;lda verticebuf_y
         ;cmp verticebuf_y+3
         ;bne is_quad
         ;ldx verticebuf_x
         ;cpx verticebuf_x+3
         ;bne is_quad

is_triangle
         ldx #$00
         ldy #$00

         ;lda verticebuf_y+0

         cmp verticebuf_y+1
         bcc +
         ldx #$01
         lda verticebuf_y+1
+
         cmp verticebuf_y+2
         bcc +
         ldx #$02
+

         lda verticebuf_y+0

         cmp verticebuf_y+1
         bcs +
         ldy #$01
         lda verticebuf_y+1
+
         cmp verticebuf_y+2
         bcs +
         ldy #$02
+
         stx y_min
         sty y_max

         ;get positions from y_min on (mod 2), looks cumbersome, but somehow necessary
         ;ldx y_min
         ;stx tmp1
         ldy modtab2+1,x
         sty tmp2
         lda modtab2+2,x
         sta tmp3

         ;ldx tmp1
         ;ldy tmp2
         jsr t1fdrawline

         ldx tmp2
         cpx y_max
         beq t_right2lns
         ldy tmp3
         jsr t1fdrawline
         jmp t_right1lns

t_right2lns
         ldy tmp2
         ldx tmp3
         jsr t2fdrawline
t_right1lns
         ldy tmp3
         ldx y_min
         ;ldx tmp1
         jsr t2fdrawline
         jmp _last

_last
         ;set load y with y_min and and set up patterns beforehand to save checks on zeropage-values later on
         ldx y_min
         ldy verticebuf_y,x

         ;end-value for y-loop
         ldx y_max

         ;setup filler depending on bank
         lda bank
         and #$08
         bne +

         ;bank 1
         lda verticebuf_y,x
         sta y2

         ;setup patterns with applied mask if pattern changed, else reuse old settings!
!ifdef USE_ILLEGALS {
         lax pattern
} else {
         ldx pattern
         txa
}
         cpx pattern_l1
         beq p_skip_1

         stx pattern_l1
         lsr
         clc
         adc #>e_patt_left
         sta e_patt2_1+2
         sta e_patt3_1+2
         adc #$02
         sta o_patt2_1+2
         sta o_patt3_1+2
         txa
         lsr
!ifdef USE_ILLEGALS {
         arr #$00
} else {
         lda #$00
         ror
}
         sta e_patt2_1+1
         sta e_patt3_1+1
         sta o_patt2_1+1
         sta o_patt3_1+1

         ;setup normal pattern without mask
         lda e_patt,x
         sta e_patt1_1+1

         lda o_patt,x
         sta o_patt1_1+1
p_skip_1
         jmp fill_1

         ;bank 2
+
         lda verticebuf_y,x
         sta y2

         ;setup patterns with applied mask if pattern changed, else reuse old settings!
!ifdef USE_ILLEGALS {
         lax pattern
} else {
         ldx pattern
         txa
}
         cpx pattern_l2
         beq p_skip_2

         stx pattern_l2
         lsr
         clc
         adc #>e_patt_left
         sta e_patt2_2+2
         sta e_patt3_2+2
         adc #$02
         sta o_patt2_2+2
         sta o_patt3_2+2
         txa
         lsr
!ifdef USE_ILLEGALS {
         arr #$00
} else {
         lda #$00
         ror
}
         sta e_patt2_2+1
         sta e_patt3_2+1
         sta o_patt2_2+1
         sta o_patt3_2+1

         ;setup normal pattern without mask
         lda e_patt,x
         sta e_patt1_2+1

         lda o_patt,x
         sta o_patt1_2+1
p_skip_2
         jmp fill_2

modtab3
         !byte $00,$01,$02,$03,$00,$01,$02
modtab2
         !byte $00,$01,$02,$00,$01

;--------------------------
;FILLER FUNCTIONS
;
;--------------------------

;----------------------
;SETUP COORDINATES
;
;----------------------

dy_is_neg
         ;in fact this case should never happen, as we always draw lines from y2 to y1 and in a wellshaped quad y2 is always greater than y1
         ;but if we get a quad like this, we can reach this part of the code and at least avoid complete failure (though not completely render the quad)
         ;      /|
         ;   -_/ |
         ;   \   |
         ;    \  |
         ;     \ |
         ;      \|
         ;yes
         ;do an abs(dy)
         eor #$ff
         adc #$01
         sta dy

         ;set x2
         lda verticebuf_x, x
         sta x2

         ;load x1 (no need to store it, as it is not used anywhere else)
         lda verticebuf_x, y

         ;and y coordinates so that y1 is always less than y2
         ldy y2
         ldx y1
         sty y1
         stx y2

         ;back to normal code
         sec
         bcs +

fcoords
         ;dynamically load vertices and get rid of indexing to have X- and Y-register free
         lda verticebuf_y, x
         sta y1
         lda verticebuf_y, y
         sta y2

         ;calc dy
         sec
         sbc y1
         ;negative?
         bcc dy_is_neg

         ;all fine, dy is positive
         ;store dy
         sta dy

         ;calc dx
         lda verticebuf_x, y
         sta x2
+
         ;sec
         ;now we have dx as well
         sbc verticebuf_x, x

         ;reset x-dir-flag (dex or inx in later loop)
         ldx #$ca ;dex

         ;dx is negative?
         bcs +

         ;yes
         ;do an abs(dx)
         eor #$ff
         adc #$01

         ;change x-direction
         ;flag that x direction must be incremented
         ldx #$e8 ;inx
+
         ;store dx
         sta dx
         rts

;--------------------------
;RENDER LINE ON TARGET 2
;
;--------------------------

!align 255,0

;now rasterize lines by calculating the x-positions for each y(scanline)-position
t2fdrawline
         ;setup x1/x2/y1/y2
         jsr fcoords
         ;fetch dy
         ldy dy
         ;dy == 0 is silly, abort
         bne +
         rts
+
         ;choose direction dx>dy or dx<dy?
         cpy dx
         bcs t2fq2

;--------------------------
;TGT2FQ1
;dx > dy x++ y--
;--------------------------

t2fq1
         ;setup inx/dex, dy, dx
         stx t2id1
         sty t2fdy1+1
         sta t2fdx1+1

         ;add offset to destination
         ldx y2
         stx t2stx1+1

         ;load startvalue for x and write first value
         lda x2
         sta target2,x
         ;setup x2
         tax

         ;load dy as counter
         ;ldy dy
         ;start with dy as err
         tya

         sec
         bcs t2fdy1
-
t2id1    inx
t2fdy1   sbc #$00
         bcs -

t2fdx1   adc #$00
         ;don't clobber Y or A, as saving and restoring those registers is fucking expensive
         dec t2stx1+1   ;6 cycles still bother me
t2stx1   stx target2
         dey
         bne -
         rts

;--------------------------
;TGT2FQ2
;dy > dx x++ y--
;--------------------------

t2fq2
         ;setup inx/dex, dy, dx
         stx t2id2
         sty t2fdy2+1
         sta t2fdx2+1

         ;add offset to destination
         ldx y2
         stx t2stx2+1

         ;load startvalue for x and write first value
         lda x2
         sta target2,x
         ;setup x2
         tax

         ;load dy as counter
         ;ldy dy
         ;start with dx as err
         lda dx

         ;TODO: too bad, nothing to optimize here like on t1fq2
         sec
-
t2fdx2   sbc #$00
         bcs +

t2fdy2   adc #$00
t2id2    inx
+
         ;don't clobber y or a, as saving and restoring those registers is fucking expensive
         dec t2stx2+1   ;6 cycles still bother me
t2stx2   stx target2
         dey
         bne -
         rts

;--------------------------
;RENDER LINE ON TARGET 1
;
;--------------------------

t1fdrawline
         ;same as above, but we write to zeropage and can thus save some more cycles
         jsr fcoords
         ;fecth dy
         ldy dy
         ;dy == 0 is silly, abort
         bne +
         rts
+
         cpy dx
         bcs t1fq2

;--------------------------
;TGT1FQ1
;dx > dy x++ y--
;--------------------------

t1fq1
         ;setup inx/dex, dy, dx
         stx t1id1
         sty t1fdy1+1
         sta t1fdx1+1

         ;add offset of 8 to x, to save subtract of 8 in inner loop later on
         lda x2
         ;clc
         adc #$08
         ldx y2
         sta target1,x
         ;setup x2
         tax

         ;add y1 to y, so we can count y to zero and save a cpy with 2 cycles
         lda #target1
         adc y1
         sta t1stx1+1

         ;load dy as counter
         ;ldy dy
         ;start with dy as err
         tya

         sec
         bcs t1fdy1
-
t1id1    inx
t1fdy1   sbc #$00
         bcs -

t1fdx1   adc #$00
         dey
         ;yay, zeropage, now we can store x directly!
t1stx1   stx target1,y
         bne -
         rts

;--------------------------
;TGT1FQ2
;dy > dx x++ y--
;--------------------------

t1fq2
         ;setup inx/dex, dy, dx
         stx t1id2
         sty t1fdy2+1
         sta t1fdx2+1

         ;add offset of 8 (7+carry) to x, to save subtract of 8 in inner loop later on
         lda x2
         ;sec
         adc #$07
         ;setup x2
         tax

         ;add y1 to y, so we can count y to zero and save a cpy with 2 cycles
         lda #target1
         adc y1
         sta tgt1+1
         sta tgt1_+1

         ;load dy as counter
         ;ldy dy
         ;start with dx as err
         lda dx

         iny
         sec
-
         dey
         ;yay, zeropage, now we can store x directly!
tgt1_    stx target1,y
         beq +

t1fdx2   sbc #$00
         bcs -
t1fdy2   adc #$00
t1id2    inx

         dey
         ;yay, zeropage, now we can store x directly!
tgt1     stx target1,y
         bne t1fdx2
+
         rts

;--------------------------
;MACROS FOR PATTERN FILLER
;
;--------------------------

;fills a line either in bank 1 or 2, and either uses the pattern for an odd or an even line
;lots of params are called by reference, so that the setup of the different code-segment works
;carry must be cleared when you enter this code
!macro fill_line ~.patt1, ~.patt2, ~.patt3, .tab, .bank {
;--------SETUP------------------------------------------------
!ifdef USE_ILLEGALS {
         lax target1,y
} else {
         lda target1,y
         tax
}
         and #$f8         ;trim to start of block (A = target1 - (target1 & 7))
         sta .bnetgt1+1   ;setup jump position - works like a charm, as A is X & $f8, just what we need as index for branch
         sbc target2,y    ;subtract right end + 1 + 8 (carry is cleared, as we always iny and compare and thus clear carry! The offset of 8 is added to target1 already)
         bcs .cmbjmp      ;result is positive? so left and right line end within same block, handle as special case (happens less often, that is why bcs is choosen to a further jmp)
;--------LEFT CHUNK-------------------------------------------
.patt2
         ;do left chunk and reuse X
         lda $1000,x      ;maskl
.bnetgt1
         ;will branch to correct block now
         bne *
         ;automated safety check (first 8 bytes can luckily be used for a check, as we have added 8 to target1 beforehand) as A must not be 0, else all steps of the jumptable would be processed, as all bne would fail.
         jmp +            ;oops, value was zero, so skip all this shit
.cmbjmp  jmp .combined    ;can even use the remaining space for our far branch for combined chunks (.combined is > $80 bytes away, else we get a target out of range)
         nop
         nop
         ;position is now * + 8
         ;generate 16 blocks with 8 bytes of mnemonics each
!for .i, 15 {
         ora .bank + (.i - 1) * $80,y
         sta .bank + (.i - 1) * $80,y
         ;XXX "lda #$xx" and then bne to "sta dst" would be nice but is not possible
         ;XXX too bad we can't reuse the line-start position for upcoming setup, as there's no more space in each block, and else the bne trick and its setup would not work
     !if .i < 15 {        ;nothing to skip on last block
         bne +
     }
}
+
;--------FULL BLOCKS AND RIGHT CHUNK--------------------------
         ;XXX this setup block still bothers me (2 lookups and nothing can be reused), but there is not much we can do about it
         ;set up jmp to speed-code segment that fills full blocks
         lda .tab,x       ;skip necessary (x1 / 8) STAs in speedcode chunk and still reuse X
         sta dst          ;set offset in speedcode segment
         ldx target2,y    ;now finally load endposition as X (will be reused for maskr,x)
         lda jmptab_cs1,x ;select speedcode segment with fitting length -> start and end of fast line filler is set
         sta dst+1        ;set highbyte of jump
.patt1
         ;load pattern
         lda #$ff
         ;now start speed-code segment that draws the remaining line including the end chunk
         jmp (dst)

;--------LEFT + RIGHT CHUNK COMBINED--------------------------
.combined
         ;left and right chunk are within same block, so combine them
         ;apply checks first
!ifdef USE_OVERFLOW_CHECK {
         ;only allow any values between 0 and 7, else something is seriously wrong (for e.g. x2<x1) and we better stop
         and #$f8
         beq *+3
         rts
}
         ;setup bne-position
!ifdef USE_ILLEGALS {
         lda #$f8
         sax .bnetgt2+1
} else {
         txa
         and #$f8
         sta .bnetgt2+1
}
         ;now put both masks together
.patt3
         lda $1000,x     ;maskl
         ldx target2,y
         and maskr,x
.bnetgt2
         ;will branch to correct block now
         bne *
         ;automated safety check again
         jmp +
         nop
         nop
         nop
         nop
         nop
         ;position is now *+8
         ;generate 16 blocks with 8 bytes of mnemonics each
!for .i, 16 {
         ora .bank + (.i - 1) * $80,y
         sta .bank + (.i - 1) * $80,y
         bne +
}
.end     rts
+
         ;advance to next line
         iny
         ;last line? (could be cpy #$xx aswell, and save one cycle, but doesn't happen so often
         cpy y2
         ;branch to next odd/even line (outside of this macro)
         beq .end
}

;--------------------------
;PATTERN FILLER BANK 1
;
;--------------------------

!align 255,0

         ;decide with which line to start with
fill_1
         tya
         lsr
         bcc even_1
         ;enter always with cleared carry
         clc
         jmp odd_1
even_1
         +fill_line ~e_patt1_1, ~e_patt2_1, ~e_patt3_1, tab3x00, bank1
odd_1
         +fill_line ~o_patt1_1, ~o_patt2_1, ~o_patt3_1, tab3x40, bank1
         jmp even_1

;--------------------------
;PATTERN FILLER BANK 2
;
;--------------------------

!align 255,0

         ;decide with which line to start with
fill_2
         tya
         lsr
         bcc even_2
         ;enter always with cleared carry
         clc
         jmp odd_2

even_2
         +fill_line ~e_patt1_2, ~e_patt2_2, ~e_patt3_2, tab3x80, bank2
odd_2
         +fill_line ~o_patt1_2, ~o_patt2_2, ~o_patt3_2, tab3xc0, bank2
         jmp even_2

;--------------------------
;JUMP TABLE FOR SPEEDCODE STARTPOSITION
;
;--------------------------


!macro build_tab3 .offset {
    !set .x = 0
    !for .i,16 {
        !for .j,8 {
            !byte .x * 3 + .offset
        }
        !set .x = .x + 1
    }
}

;jumptables for start position in corresponding speedcode-chunk (block * 3 + offset)
!align 127,0
tab3x00
         +build_tab3 $00

tab3x40
         +build_tab3 $40

tab3x80
         +build_tab3 $80

tab3xc0
         +build_tab3 $c0

;--------------------------
;JUMPCODE FOR ALL BANKS AND PATTERNS
;
;--------------------------

!macro fill_line .bank, .size, .offset {
    !set .s = 0
    !do while .s < .size {
        sta .bank + .s * .offset,y
        !set .s = .s + 1
    }
    and maskr,x
    ora .bank + .s * .offset,y
    sta .bank + .s * .offset,y
}

;generate speedcode segments aligned to a $40-boarder,
;all segments for bank1 start at $xx00 (even) and $xx40 (odd), bank2 is at $xx80 and $xxc0. The offsets are already added to the tables that represent a multiple of 3 (tab3xx)

;the speedcodesegments are arranged like the following:

;$x000 bank1 even 0 pixel wide
;$x040 bank1 odd  0 pixel wide
;$x080 bank2 even 0 pixel wide
;$x0c0 bank2 odd  0 pixel wide
;
;$x100 bank1 even 8 pixel wide
;$x140 bank1 odd  8 pixel wide
;$x180 bank2 even 8 pixel wide
;$x1c0 bank2 odd  8 pixel wide
;
;...
;
;$xf00 bank1 even 120 pixel wide
;$xf40 bank1 odd  120 pixel wide
;$xf80 bank2 even 120 pixel wide
;$xfc0 bank2 odd  120 pixel wide

;thus we can influence the length of the line by choosing the highbyte properly, and choose the start of a line, by jumping into the segment with an offset of startpos * 3

!align 255,0
speed_code

!for .x, 15 {

!align 63,0
         +fill_line bank1, .x, $80
         iny
         cpy y2
         +lbne odd_1
         rts

!align 63,0
         +fill_line bank1, .x, $80
         iny
         cpy y2
         +lbne even_1
         rts

!align 63,0
         +fill_line bank2, .x, $80
         iny
         cpy y2
         +lbne odd_2
         rts

!align 63,0
         +fill_line bank2, .x, $80
         iny
         cpy y2
         +lbne even_2
         rts
}

!align 255,0
e_patt_left
!fill 512,0

o_patt_left
!fill 512,0

!align 127,0
jmptab_cs1

;generate a table of highbytes of each segment's address.
!set .i = -1
!for .x,16 {
    !for .y,8 {
         !byte (>speed_code) + .i
    }
    !set .i = .i + 1
}

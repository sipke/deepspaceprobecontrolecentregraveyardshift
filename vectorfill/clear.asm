;--------------------------
;CLEAR
;clear screenbuffer, FAST and only necessary areas
;--------------------------

;clear only those blocks that the object rotates in (circle with a radius of 128)
;takes ~$57 rasterlines, so not much to bother about

clear1  = $8000
clear2  = $a000

clear
         lda bank
         and #$08
         bne cl_bank2
         jmp clear1
cl_bank2
         lda #$00
         jmp clear2

val1     = 43
val2     = 28
val3     = 19
val4     = 13
val5     = 8
val6     = 4
val7     = 3
val8     = 1

tab_s
         !byte val1, val2, val3, val4, val5, val6, val7, val8
         !byte val8, val7, val6, val5, val4, val3, val2, val1
tab_e
         !byte 127-val1, 127-val2, 127-val3, 127-val4, 127-val5, 127-val6, 127-val7, 127-val8
         !byte 127-val8, 127-val7, 127-val6, 127-val5, 127-val4, 127-val3, 127-val2, 127-val1

gen_clear
         lda #<clear1
         sta dst
         lda #>clear1
         sta dst+1
         ldy #$00

         lda #<bank1
         sta src
         lda #>bank1
         sta src+1
         jsr make_bank

         lda #<clear2
         sta dst
         lda #>clear2
         sta dst+1
         ldy #$00

         lda #<bank2
         sta src
         lda #>bank2
         sta src+1
         jmp make_bank

make_bank
         ldx #$00
--
         lda tab_e,x
         ora src
         sta cl_lst+1
         lda tab_s,x
         ora src
         sta src
-
         lda #$8d
         sta (dst),y
         iny
         bne +
         inc dst+1
+
         lda src
         sta (dst),y
         iny
         bne +
         inc dst+1
+
         lda src+1
         sta (dst),y
         iny
         bne +
         inc dst+1
+
         inc src
         lda src
cl_lst   cmp #$00
         bne -

         and #$80
         eor #$80
         sta src
         bmi +
         inc src+1
+
         inx
         cpx #$10
         bne --

         lda #$60
         sta (dst),y
         rts

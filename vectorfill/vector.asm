!cpu 6510
!convtab scr

         *= $3000

;x1       = $30
x2       = $31
y1       = $32
y2       = $33

dx       = $34
dy       = $35

z1       = $36
z2       = $38
z3       = $3a
z4       = $3c
z5       = $3e
z6       = $40
z7       = $42
z8       = $44

mesh     = $46

bank     = $47

tmp1     = $48
tmp2     = $49
tmp3     = $4a
tmp4     = $4b

pattern  = $4c
col      = $4d

num_fills= $4e

deg_x    = $4f
deg_y    = $50
deg_z    = $51

y_min    = $52
y_max    = $53

pattern_l1= $54
pattern_l2= $55
face_first= $56
face_last = $57
face_offs = $58

src      = dx
dst      = $5a

verticebuf_x = $60
verticebuf_y = $64

USE_ILLEGALS
USE_OVERFLOW_CHECK
;BENCHMARK

target1 = $68
target2 = $0100

bank1   = $2000
bank2   = $2800

!ifdef BENCHMARK {
fcnt_l  = $03fe
fcnt_h  = $03ff
}

;transformed vertices go here:
vertices_x = $0280
vertices_y = $02c0
vertices_z = $0300
bf_cache   = $0340

maskr   = $0200

;load some generic macros and math stuff
!src "macros.asm"
!src "maths.asm"

;--------------------------
;MAINCODE
;
;--------------------------

         jsr gen_clear
         ;init irq
         sei
         lda #$35
         sta $01
         +irq_setup
         +irq_set_vector irq1, $32
         cli

         jsr screen_setup

         ;prepare highbytes of mathtable-pointers
         lda #>tmath1
         sta z1+1
         sta z3+1
         sta z5+1
         sta z7+1
         sta z1_+2
         sta z3_+2
         sta z1__+2
         sta z3__+2
         lda #>tmath2
         sta z2+1
         sta z4+1
         sta z6+1
         sta z8+1
         sta z2_+2
         sta z4_+2
         sta z2__+2
         sta z4__+2

         ;setup fade position
         lda #$04
         sta col

         ;set pattern cache to some values !=0
         sta pattern_l1
         sta pattern_l2

         ;generate mask tables
         jsr create_masks

         ;clear a bunch of variables
         lda #$00
         sta mesh
         sta deg_x
         sta deg_y
         sta deg_z
         sta num_fills
         sta dst
         sta dst+1

!ifdef BENCHMARK {
         ;reset frame counter
         sta fcnt_l
         sta fcnt_h
}

         ;load first mesh
         jsr load_mesh

start
         ;fetch values from sinus and cosinus-table per angle
         +fetch_sin_cos

         ;project/transform all vertices
last_vert
         ldx #$00
-
         +project ~vertx, ~verty, ~vertz
         dex
         +lbpl -

         ;advance angles
         inc deg_x
         inc deg_x
         inc deg_x
         inc deg_y
         inc deg_y
         inc deg_z

         ;draw this sucker
to_draw  jsr all_faces

         ;switch bank
         lda bank
         eor #$08
         sta bank

!ifdef BENCHMARK {
         ;switch $d018 on our own, or let the raster-irq do it?
         ldx $d012
         cpx #$e0
         bcs +
}
         ;set $d018
         lsr
         lsr
         eor #$12
         sta $d018
!ifdef BENCHMARK {
+
}
         ;clear new working buffer
         jsr clear

         ;increment some counter
         inc num_fills

         ;decide if we just fade in/out or continue as normal
         lda num_fills
         cmp #$07
         bcc fadein
         cmp #$f8
         bcs fadeout
         jmp start
fadeout
         ;fade out old mesh
         inc col
         lda col
         cmp #$01
         bne +
         ;ldx mesh
         ;lda modtab3+1,x
         lda #0
         sta mesh
         jsr load_mesh

!ifdef BENCHMARK {
         ;print benchmark result
         lda fcnt_h
         and #$0f
         jsr hex
         sta $07e0
         lda fcnt_l
         lsr
         lsr
         lsr
         lsr
         jsr hex
         sta $07e1
         lda fcnt_l
         and #$0f
         jsr hex
         sta $07e2
}
         ;reset some variables
         lda #$00
!ifdef BENCHMARK {
         sta fcnt_l
         sta fcnt_h
}
         sta num_fills
         sta deg_x
         sta deg_y
         sta deg_z
+
         jmp start
fadein
         ;fade in new mesh
         lda col
         beq +
         dec col
+
         jmp start

screen_setup
         ;set dark gray as color
         ldy #$00
         lda #$0b
-
         sta $d800,y
         sta $d900,y
         sta $da00,y
         sta $db00,y
         dey
         bne -

         lda #$20
         sta bank
         lda #$18
         sta $d018

         ;fill screen
         lda #$ff
-
         sta $0400,y
         sta $0500,y
         sta $0600,y
         sta $0700,y
         dey
         bne -

!ifdef BENCHMARK {
         ldy #$27
-
         lda info,y
         sta $07c0,y
         dey
         bpl -
}

         ;clear doublebuffer
         ldy #$00
         sty dst
         lda #$20
         sta dst+1
         ldx #$10
         tya
-
         sta (dst),y
         dey
         bne -
         inc dst+1
         dex
         bne -

         ;build 16x16 charset grid
--
         ldy #$00
         txa
-
scr_     sta $0484,y
         clc
         adc #$10
         iny
         cpy #$10
         bne -
         lda scr_+1
         clc
         adc #$28
         sta scr_+1
         bcc +
         inc scr_+2
+
         inx
         cpx #$10
         bne --
         rts

!ifdef BENCHMARK {
hex
         tax
         lda hextab,x
         rts

hextab
         !text "0123456789abcdef"

info
         !text "  frames needed for 256 fills: $        "
}

;------------------------
;DRAW ALL FACES
;
;------------------------

all_faces
         ;reset cache indexes
         lda #<bf_cache
         sta cache_i1+1
         sta cache_i2+1

         ;the cheap and dirty approach:
         ;draw back faces first, so the front faces can paint over the already drawn back faces

         ;start of face-list
         ldy face_first
bf1
         ;set new offset in face-list
         sty face_offs
         ;load vertices clockwise and check if face is hidden
         ;buf[0] = vertice[0]
         ldx faces+0,y
         lda vertices_x,x
         sta verticebuf_x+0
         lda vertices_y,x
         sta verticebuf_y+0

         ;buf[2] = vertice[2]
         ldx faces+2,y
         lda vertices_x,x
         sta verticebuf_x+2
         ;set up first factor for vector-product
         sec
         sbc verticebuf_x+0
         sta z1__+1
         eor #$ff
         sta z2__+1

         lda vertices_y,x
         sta verticebuf_y+2
         ;set up second factor for vector-product
         sec
         sbc verticebuf_y+0
         sta z3__+1
         eor #$ff
         sta z4__+1

         ;buf[1] = vertice[1]
         ldx faces+1,y
         lda vertices_y,x
         ;multiplicand 1
         sta verticebuf_y+1
         sec
         sbc verticebuf_y+2
         tay

         lda vertices_x,x
         ;multiplicand 2
         sta verticebuf_x+1
         sec
         sbc verticebuf_x+2
         tax

         ;subtract both products
         sec
z3__     lda $1000,x
         sec
z4__     sbc $1000,x
         sec
z1__     sbc $1000,y
         clc
z2__     adc $1000,y

         ;cache result for front face check
cache_i1 sta bf_cache
         bpl skip_face_b

         ;buf[3] = vertice[3]
         ldy face_offs
         ;ldx faces+3,y
         ; make vertice 3 = vertice 0
         ldx faces+0,y
         lda vertices_x,x
         sta verticebuf_x+3
         lda vertices_y,x
         sta verticebuf_y+3

         ;now draw face
         ;first load pattern
         ;lda faces+4,y
         lda faces+3,y
         clc

         ;respect current fading situation
         sbc col
         sec

         ;and make backfaces appear brighter
         sbc #$02

         ;is it too bright to be drawn?
         bmi skip_face_b

         ;nope, draw it
         sta pattern
         jsr drawface

skip_face_b
         ;increment cache index
         inc cache_i1+1
         ;advance face-list offset
         lda face_offs
         clc
         ;adc #$05
         adc #$04
         tay
+
         cmp face_last
         +lbne bf1

         ;restore face-list offset
         ldy face_first
ff1
         sty face_offs

         ;just reuse calculated values from first run
cache_i2 lda bf_cache
         ;it is a front face?
         bmi skip_face_f1

         ;load vertices counter-clockwise, face is visible, no further checks needed
         ;buf[0] = vertice[2]
         ldx faces+2,y
         lda vertices_x,x
         sta verticebuf_x+0
         lda vertices_y,x
         sta verticebuf_y+0

         ;buf[1] = vertice[1]
         ldx faces+1,y
         lda vertices_x,x
         sta verticebuf_x+1
         lda vertices_y,x
         sta verticebuf_y+1

         ;buf[2] = vertice[0]
         ldx faces,y
         lda vertices_x,x
         sta verticebuf_x+2
         lda vertices_y,x
         sta verticebuf_y+2

         ;buf[3] = vertice[0]
         ;ldx faces+0,y
         ;lda vertices_x,x
         ;sta verticebuf_x+3
         ;lda vertices_y,x
         ;sta verticebuf_y+3

         ;load pattern from mesh
         ;lda faces+4,y
         lda faces+3,y
         clc
         sbc col
         bmi skip_face_f1
-
         sta pattern
         jsr drawface

skip_face_f1
         ;increment cache idnex
         inc cache_i2+1
         ;advance pointer
         lda face_offs
         clc
         ;adc #$05
         adc #$04
         tay
+
         cmp face_last
         bne ff1
         rts

;------------------------
;DRAW FRONT FACES ONLY
;
;------------------------

front_faces
         ldy face_first
ff2
         ;index to current face
         sty face_offs
         ;load vertices counter-clockwise and check if face is hidden
         ;buf[0] = vertice[2]
         ldx faces+2,y
         lda vertices_x,x
         sta verticebuf_x+0
         lda vertices_y,x
         sta verticebuf_y+0

         ;buf[1] = vertice[1]
         ldx faces+1,y
         lda vertices_x,x
         sta verticebuf_x+1
         sec
         sbc verticebuf_x+0

         ;set up first factor
         sta z1_+1
         eor #$ff
         sta z2_+1

         lda vertices_y,x
         sta verticebuf_y+1
         sec
         sbc verticebuf_y+0

         ;set up second factor
         sta z3_+1
         eor #$ff
         sta z4_+1

         ;buf[2] = vertice[0]
         ldx faces,y
         lda vertices_x,x
         sta verticebuf_x+2
         sec
         sbc verticebuf_x+1

         ;multiplier 1
         tay

         lda vertices_y,x
         sta verticebuf_y+2
         sec
         sbc verticebuf_y+1

         ;multiplier 2
         tax

         ;multiply first and subtract second
z3_      lda $1000,y
         sec
z4_      sbc $1000,y
         sec
z1_      sbc $1000,x
         clc
z2_      adc $1000,x

         ;stop asap if face is hidden
         bmi skip_face_f2

         ;face is visible
         ;remember value for shading
         sta tmp3

         ;load last remaining vertice
         ;buf[3] = vertice[0]
         ;ldy face_offs     ;Y got clobbered, so fetch index again
         ;ldx faces+0,y
         ;lda vertices_x,x
         ;sta verticebuf_x+3
         ;lda vertices_y,x
         ;sta verticebuf_y+3

fetch_end2
         ;last mesh (checkerboard-cube)?
draw_mode
         jmp shaded
shaded
         ;get result of vector-product again
         lda tmp3
         ;scale length of product and use it for shading
         lsr
         lsr
         lsr
         clc
         sbc col
         bmi skip_face_f2

         cmp #$04
         bcc +
         lda #$03
         bne +

unshaded
         ;draw face
         ;load pattern from mesh
         ;lda faces+4,y
         lda faces+3,y
         clc
         sbc col
         bmi skip_face_f2
+
         sta pattern
         jsr drawface

skip_face_f2
         ;advance pointer
         lda face_offs
         clc
         ;adc #$05
         adc #$04
         tay
         cmp face_last
         +lbne ff2
         rts


;------------------------
;LOAD MESH
;
;------------------------

load_mesh
         ;set pointer (16bit) to mesh
         asl
         tax
         lda meshtab,x
         sta dst
         lda meshtab+1,x
         sta dst+1

         ;get sizes for vertice-list and set it up
         ldy #$00
         lda (dst),y
         sec
         sbc #$01
         sta last_vert+1
         iny

         ;setup things for backface culling (to avoid a check per face)
         lda (dst),y
         beq +
         ;draw front faces only
         lda #<front_faces
         sta to_draw+1
         lda #>front_faces
         sta to_draw+2
         jmp ++
+
         ;also draw back faces
         lda #<all_faces
         sta to_draw+1
         lda #>all_faces
         sta to_draw+2
++
         iny
         lda (dst),y
         beq +
         lda #<shaded
         sta draw_mode+1
         lda #>shaded
         sta draw_mode+2
         jmp ++
+
         lda #<unshaded
         sta draw_mode+1
         lda #>unshaded
         sta draw_mode+2
++
         iny
         ;set pointers (16bit) to x/y/z-part of vertices
         lda (dst),y
         sta vertx+1
         iny
         lda (dst),y
         sta vertx+2

         iny
         lda (dst),y
         sta verty+1
         iny
         lda (dst),y
         sta verty+2

         iny
         lda (dst),y
         sta vertz+1
         iny
         lda (dst),y
         sta vertz+2

         ;set start and end position of face list
         iny
         lda (dst),y
         sta face_first
         iny
         lda (dst),y
         sta face_last
         rts

;------------------------
;IRQ HANDLER
;
;------------------------

irq1
         ;handle irq with all roms off
         +irq_enter
         ;set $d018
         lda bank
         lsr
         lsr
         ora #$10
         eor #$02
         sta $d018
         nop
         nop
         nop
         ;change background color
         lda #$0c
         sta $d020
         sta $d021
         ;set up next irq
!ifdef BENCHMARK {
         +irq_set_vector irq2, $f2
} else {
         +irq_set_vector irq2, $fa
}
         ;exit irq
         +irq_exit
irq2
         +irq_enter
!ifdef BENCHMARK {
         ldx #$02
         dex
         bpl *-1
         ldx #$16
         lda #$00
         stx $d018
         sta $d020
         sta $d021
         inc fcnt_l
         bne *+5
         inc fcnt_h
} else {
         ldx #$03
         dex
         bpl *-1
         lda #$00
         sta $d020
         sta $d021
}
         +irq_set_vector irq1, $32
         +irq_exit

e_patt
         !byte $11,$aa,$ee,$ff
o_patt
         !byte $44,$55,$bb,$ff

!src "meshes.asm"

!src "math_tables.asm"

!src "clear.asm"

!src "fill.asm"

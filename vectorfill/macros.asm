;--------------------------
;MACROS
;some handy macros for irq handling and non existent mnemonics
;--------------------------

tmpa    = $25
tmpx    = $26
tmpy    = $27
tmp_1   = $28

!macro lbmi .target {
        bpl *+5
        jmp .target
}

!macro lbpl .target {
        bmi *+5
        jmp .target
}

!macro lbeq .target {
        bne *+5
        jmp .target
}

!macro lbne .target {
        beq *+5
        jmp .target
}

!macro irq_enter {
        sta tmpa
        stx tmpx
        sty tmpy
        lda $01
        sta tmp_1
        lda #$35
        sta $01
        dec $d019
}

!macro irq_exit {
        lda $dc0d
        lda tmp_1
        sta $01
        ldy tmpy
        ldx tmpx
        lda tmpa
        rti
}

!macro irq_setup {
        lda #$7f
        sta $dc0d
        lda #$01
        sta $d01a
        sta $d019
}

!macro irq_set_vector .vector, .line {
        lda #<.vector
        sta $fffe
        lda #>.vector
        sta $ffff
        lda #.line
        sta $d012
        lda $d011
        and #$3f
        sta $d011
}

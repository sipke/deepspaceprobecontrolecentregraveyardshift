meshtab
         !word mesh1, mesh2

C0 = 0.927050983124842272306880251548; = 3 * (sqrt(5) - 1) / 4
C1 = 1.33058699733550141141687582919;  = 9 * (9 + sqrt(5)) / 76
C2 = 2.15293498667750705708437914596;  = 9 * (7 + 5 * sqrt(5)) / 76
C3 = 2.427050983124842272306880251548; = 3 * (1 + sqrt(5)) / 4
fact = 3


faces
;here faces are made up by 3 points ABC with fill pattern P 
face1
  !byte 12, 0, 2, 1
  !byte 12, 2, 26, 1
  !byte 12, 26, 4, 1
  !byte 12, 4, 24, 1
  !byte 12, 24, 0, 1
  !byte 13, 3, 1, 1
  !byte 13, 1, 25, 1
  !byte 13, 25, 5, 1
  !byte 13, 5, 27, 1
  !byte 13, 27, 3, 1
  !byte 14, 2, 0, 1
  !byte 14, 0, 28, 1
  !byte 14, 28, 6, 1
  !byte 14, 6, 30, 1
  !byte 14, 30, 2, 1
  !byte 15, 1, 3, 1
  !byte 15, 3, 31, 1
  !byte 15, 31, 7, 1
  !byte 15, 7, 29, 1
  !byte 15, 29, 1, 1
  !byte 16, 4, 5, 1
  !byte 16, 5, 25, 1
  !byte 16, 25, 8, 1
  !byte 16, 8, 24, 1
  !byte 16, 24, 4, 1
  !byte 17, 5, 4, 1
  !byte 17, 4, 26, 1
  !byte 17, 26, 9, 1
  !byte 17, 9, 27, 1
  !byte 17, 27, 5, 1
  !byte 18, 7, 6, 1
  !byte 18, 6, 28, 1
  !byte 18, 28, 10, 1
  !byte 18, 10, 29, 1
  !byte 18, 29, 7, 1
  !byte 19, 6, 7, 1
  !byte 19, 7, 31, 1
  !byte 19, 31, 11, 1
  !byte 19, 11, 30, 1
  !byte 19, 30, 6, 1
  !byte 20, 8, 10, 1
  !byte 20, 10, 28, 1
  !byte 20, 28, 0, 1
  !byte 20, 0, 24, 1
  !byte 20, 24, 8, 1
  !byte 21, 10, 8, 1
  !byte 21, 8, 25, 1
  !byte 21, 25, 1, 1
  !byte 21, 1, 29, 1
  !byte 21, 29, 10, 1
  !byte 22, 11, 9, 1
  !byte 22, 9, 26, 1
  !byte 22, 26, 2, 1
  !byte 22, 2, 30, 1
  !byte 22, 30, 11, 1
  !byte 23, 9, 11, 1
  !byte 23, 11, 31, 1
  !byte 23, 31, 3, 1
  !byte 23, 3, 27, 1
  !byte 23, 27, 9, 1

face2

meshes
mesh1
         ;dodacohedron
         ;num vertices
         !byte 60
         ;backface culling
         !byte 1
         ;shaded
         !byte 0
         ;pointers to vertices
         !word vertx1
         !word verty1
         !word vertz1
         ;start pos of faces
         !byte face1-face1
         ;end pos of faces
         !byte face2-face1
         ;f3 = 59


mesh2

vertices

vertx1 
   !byte 0.0 * fact 
   !byte 0.0 * fact 
   !byte 0.0 * fact 
   !byte 0.0 * fact 
   !byte C3 * fact 
   !byte C3 * fact 
   !byte -C3 * fact 
   !byte -C3 * fact 
   !byte C0 * fact 
   !byte C0 * fact 
   !byte -C0 * fact 
   !byte -C0 * fact 
   !byte C1 * fact 
   !byte C1 * fact 
   !byte -C1 * fact 
   !byte -C1 * fact 
   !byte C2 * fact 
   !byte C2 * fact 
   !byte -C2 * fact 
   !byte -C2 * fact 
   !byte 0.0 * fact 
   !byte 0.0 * fact 
   !byte 0.0 * fact 
   !byte 0.0 * fact 
   !byte 1.5 * fact 
   !byte 1.5 * fact 
   !byte 1.5 * fact 
   !byte 1.5 * fact 
   !byte -1.5 * fact 
   !byte -1.5 * fact 
   !byte -1.5 * fact 
   !byte -1.5 * fact 

verty1 
   !byte C0 * fact 
   !byte C0 * fact 
   !byte -C0 * fact 
   !byte -C0 * fact 
   !byte 0.0 * fact 
   !byte 0.0 * fact 
   !byte 0.0 * fact 
   !byte 0.0 * fact 
   !byte C3 * fact 
   !byte -C3 * fact 
   !byte C3 * fact 
   !byte -C3 * fact 
   !byte 0.0 * fact 
   !byte 0.0 * fact 
   !byte 0.0 * fact 
   !byte 0.0 * fact 
   !byte C1 * fact 
   !byte -C1 * fact 
   !byte C1 * fact 
   !byte -C1 * fact 
   !byte C2 * fact 
   !byte C2 * fact 
   !byte -C2 * fact 
   !byte -C2 * fact 
   !byte 1.5 * fact 
   !byte 1.5 * fact 
   !byte -1.5 * fact 
   !byte -1.5 * fact 
   !byte 1.5 * fact 
   !byte 1.5 * fact 
   !byte -1.5 * fact 
   !byte -1.5 * fact 

vertz1 
   !byte C3 * fact 
   !byte -C3 * fact 
   !byte C3 * fact 
   !byte -C3 * fact 
   !byte C0 * fact 
   !byte -C0 * fact 
   !byte C0 * fact 
   !byte -C0 * fact 
   !byte 0.0 * fact 
   !byte 0.0 * fact 
   !byte 0.0 * fact 
   !byte 0.0 * fact 
   !byte C2 * fact 
   !byte -C2 * fact 
   !byte C2 * fact 
   !byte -C2 * fact 
   !byte 0.0 * fact 
   !byte 0.0 * fact 
   !byte 0.0 * fact 
   !byte 0.0 * fact 
   !byte C1 * fact 
   !byte -C1 * fact 
   !byte C1 * fact 
   !byte -C1 * fact 
   !byte 1.5 * fact 
   !byte -1.5 * fact 
   !byte 1.5 * fact 
   !byte -1.5 * fact 
   !byte 1.5 * fact 
   !byte -1.5 * fact 
   !byte 1.5 * fact 
   !byte -1.5 * fact 


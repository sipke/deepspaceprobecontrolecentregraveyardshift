#importonce


//global memory map

//3000 sprites ~20 
//3800 fake russian font
//----
//4000 VIC bank photos
//----
//8000 tables
//8000  36 *5  =180 bytes of angle values in floating point radians in steps of PI/36
//8100 lo pointer for angle values
//8200 hi pointer for angles values
//8f00 in game variables

//9800 -9fff photo work area 
//c000-c400 circle memories
//c400 startpoints
//c500 end points
//c600 switch points
//c800 - ca00 circle x coordinates
//ca00 - cb00 temporary stuff
//cb00 - cc00 ytable dots
//ce00 text pointers lo
//ce80 text pointers hi
//cf00 - d000 other variables

// con
//==================== inGAME CALCS=============================================

.const PROBE_SPEED = 100    //is km/s
.const FRAMES_PER_SECOND = 50
.const PROBE_SPEED_PER_FRAME = PROBE_SPEED/FRAMES_PER_SECOND
.const MAX_NR_PHOTOS= 6
.const LIGHT_SPEED = 299792

.const angleWritePos = $684
.const earthDistWritePos = $60f
.const planetDistWritePos = $4f7

.const sentWritePos = $2e2 + $400 -12
.const receivedWritePos = $2e2 + $400


.const PHOTO_MEM_BASE = $4000

.const angleFACTable= $8000
.const angleTablePointerLo = $8100
.const angleTablePointerHi = $8200
.const intAnglePetsciiTable = $8300  //36*4 bytes
.const decToPetsciiTableLo = $8400  //256 bytes
.const decToPetsciiTableHi = $8500  //256 bytes

//8 bytes per photo reserved only 7 used, but easier retrieval by index, max 8 photos
.const photoAtDist = $8e00   //-$8e40
//a bytes per photo, is index to table

.const PHOTO_STATE_TAKEN = 1
.const PHOTO_STATE_OPEN = 0
.const PHOTO_STATE_DEVELOPING = 2
.const PHOTO_STATE_READY = 3

.const D018_BASE_VALUE = $01
.const D018_MIN_VALUE = 03
.const D018_MAX_VALUE = 13

//----------------------- JOYSTICK
.const JOY1_FIRE = 239
.const JOY1_LEFT = 251
.const JOY1_RIGHT = 247
.const JOY2_FIRE = 111
.const JOY2_LEFT = 123
.const JOY2_RIGHT = 119

//-------------------------- COLOR PETSCII CODES
.const COL_CODE_WHITE = 5
.const COL_CODE_RED = 28
.const COL_CODE_GREEN = 30
.const COL_CODE_BLUE = 31
.const COL_CODE_ORANGE = 129
.const COL_CODE_BLACK = 144
.const COL_CODE_BROWN = 149
.const COL_CODE_LIGHT_RED = 150
.const COL_CODE_LIGHT_GREY = 151
.const COL_CODE_GREY = 152
.const COL_CODE_LIGHT_GREEN = 153
.const COL_CODE_LIGHT_BLUE = 154
.const COL_CODE_DARK_GREY = 155
.const COL_CODE_PURPLE = 156
.const COL_CODE_YELLOW = 158
.const COL_CODE_CYAN = 159
//------------------------------
// 
.const PI = $AEA8
.const HALF_PI = $E2E0
.const HALF = $BF11

.const FAC_X = $c000 + 0


.const FAC_ANGLE_RAD = $c000 + 10
.const FAC_ANGLE_DEG = $c000 + 15

.const FAC_FOV = HALF_PI

//.const FAC_SCREEN_SIZE = $c000 + 30
//.const FAC_SCREEN_SIZE_DIV2 = $c000 + 35
.const FAC_PLANET_MAX_SIZE = $c000 + 40
.const FAC_PLANET_SIZE = $c000 + 45
.const FAC_WORK_AREA = $c000 + 50
.const FAC_X_SQUARED = $c000 + 55
.const FAC_SQUARED_SUM = $c000 + 60
.const FAC_DISTANCE = $c000 + 65
.const FAC_PLANET_SIZE_DIV2 = $c000 + 70
.const FAC_START_Y = $c000 + 75
.const FAC_ANGLE_PLANET_RAD = $c000 + 80
.const FAC_ANGLE_DIV = $c000 + 85
.const FAC_PLANET_X = $c000 + 90
.const planetViewX = $c000 + 95
.const FAC_END_Y= $c000 + 100
.const startYInt= $c000 + 105   //is really just one byte, but this won't break the pattern. Post load mem doesn't reallty matter anyway. Lots of that.
.const endYInt= $c000 + 110 //is really just one byte, but this won't break the pattern.Post load mem doesn't reallty matter anyway. Lots of that.
//y coordinate of photo
.const INT_Y    = $c000 +111
//general temporart value

.const FAC_TMP = $c000 +115
.const FAC_TMP2 = $c000 +120
//width of planet divided by width of screen
.const FAC_WIDTH_FACTOR = $c000 + 125
//value between 0-1 where dark side switches to light side
.const FAC_SWITCH_FACTOR = $c000 + 130
.const INT_TMP0 = $c000 + 135
.const INT_TMP1 = $c000 + 136
.const INT_TMP2 = $c000 + 137
.const INT_TMP3 = $c000 + 138
.const INT_TMP4 = $c000 + 139
//x coordinate of centre of planet on photo
.const FAC_MIDPOINT = $c000 + 140
//x coordinate of left side of planet on photo
.const FAC_STARTPOINT = $c000 + 145
//x coordinate of right side of planet on photo
.const FAC_ENDPOINT = $c000 + 150
//width of planet on photo
.const FAC_LINE_WIDTH = $c000 + 155
//x coordinate where dark side becomes the light side on photo
.const FAC_SWITCHPOINT = $c000 + 160
//half width of planet on photo
.const FAC_LINE_WIDTH_DIV2 = $c000 + 165
//if below zero, needed for calculation of switch point
.const FAC_REAL_STARTPOINT = $c000 + 170
//Y coordinate of planet in relation to probe
.const INT16_DELTA_TIME = $c000 + 175	//2 bytes
.const Y = 1000
.const Y_SQUARED = Y*Y

.const FAC_1 = $61
.const FAC_2 = $69

//.const SCREEN_SIZE= 128
//.const SCREEN_SIZE_DIV2= 64
//.const PLANET_SIZE = 128

//--------------memory map -----------------------
// storing floating point multiplier for drawing a quarter circle, 64 compressed FACs
.const CIRCLE_X_TABLE = $c800   //to aprox ca00
//------------ //9a00
.const START_POINTS = $c400 //-9100
.const END_POINTS = $c500 //-9200
.const SWITCH_POINTS = $c600 //-9300

.const CHARS_AT = $3800

//--------------------  DOTS DRAWING ----------------------------------

.const PHOTO_CHARS_BASE = $4800
.const PHOTO_SCREEN_MEM= $4000 //1000 bytes
.const PHOTO_WORK_AREA = $9800

.const WRITE_ADDRESS_ZP = $fc
//also $fd 
//begin and end coordinate of line
.const X1 = $fe
.const X2 = $ff
.const mask = $ca00
//y coordinate of line
//.const Ytemp= $c801
//individual pixel x,y
.const xDot= $ca02
.const yDot= $ca03

.const count = $ca04

.const tempByte = $ca05

.const eventType = $ca06
.const textAtX = $ca07
.const textAtY = $ca08
.const eventDurationHi = $ca09
.const eventDurationLo = $ca0a
.const currentEventIndex = $ca0b
.const eventParameter = $ca0c

.var MAX_EVENTS

.const temp16bytes = $ca20

.const yTable = $cb00 //- ca00

//------------------------- STATE ---------------
.const STATE_STARTUP = 0
.const STATE_WAIT_FOR_USER = 1
.const STATE_INTRO_1 =2
.const STATE_INTRO_2 =3
.const STATE_INGAME =4
.const STATE_RECAP =5
.const STATE_SCORE =6
.const STATE_SET_EVENT = 7
.const STATE_WAIT_FOR_LAST_DEVELOPMENT = 8
.const STATE_SHOWING_PHOTOS = 9
.const STATE_SHOWING_PHOTOS_ALL_SHOWN = 10
.const STATE_RESOLUTION = 11

.const IRQ_LINE = $0
.const IRQ_LINE2 = 202

.const textPointersLo = $ce00
.const textPointersHi = $ce80

.const TEXT_X_OFFSET = 0
.const TEXT_Y_OFFSET = 4

.const LINE_Y = 15
.const LINE_CHAR = 100
.const LINE_LENGTH = 20
.const LINE_OFFSET = 10

//----------------------- VARIABLE SPACE ------------------
//============= ZERO BASED CONSTANTS
// could be grouped together so they are easily initialized in a loop to save space
/*
	stx TMP
*/

.const BASE_ZERO_CONSTANTS_START = $cf00
.const state = $cf00
.const doStateChange = $cf01
.const previousTextLength = $cf02//used for clearing previous text
.const isAnimating = $cf03
.const animationCounter = $cf04
//
.const repeats = $cf05

.const photoAtAngleIndex = $cf06 //
.const photoAtDistStringTemp = $cf07    // max 16 positions
.const photoAtAngleFACTemp = $cf17    // 5 poisitions
.const photoAtDistFACTemp = $cf1c    // 5 positions
.const photoInStructionSentAtTime = $cf21   //nr of photos times 2 max 16

.const currentIntAngle = $cf31
.const currentAngleIndex = $cf32
.const prevJoyValue = $cf33
// to determine if photo can be taken is calculating, is calculated
.const photoStatus = $cf34 //-8f0f  max nr of photos unknown but no more than 8, really

.const distProbeToEarthFac = $cf40
.const distProbeToPlanetFac = $cf45
.const distanceEarthToPlanet = $cf4a

.const frameCountLo = $cf50
.const frameCountHi = $cf51

//for fast printing
//.const distProbeToEarthDecimal = $cf52   //4 bytes, 7 digits  plus sign
//.const distProbeToPlanetDecimal = $cf56   //3 bytes, 5 digits  plus sign

.const curPhotoHandling = $cf5b
.const lastPhotoTaken = $cf5c
.const score =          $cf5d // 2 bytes
.const photosDeveloped = $cf5f
.const photosReturned = $cf60

.const photoEventReceivedAt = $cf61 //max 8 events of 2 bytes- cf71
.const TMP = $cf72
.const tmpTime = $cf7e //2 bytes
.const index5 = $cf80 

.const photosViewed = $cf90
.const viewingPhoto = $cf91

//------------------------ SPRITES ----------------------
.const SPRITE_MEM_BASE = $3000
.const SPRITE_POINTER_BASE = SPRITE_MEM_BASE/64
.const PAVEL_SWEEP_0_TOP = 0 + SPRITE_POINTER_BASE
.const PAVEL_SWEEP_1_TOP = 2 + SPRITE_POINTER_BASE
.const PAVEL_KNEEL_TOP = 4 + SPRITE_POINTER_BASE
.const BROOM_SWEEP_0_TOP = 1 + SPRITE_POINTER_BASE
.const BROOM_SWEEP_1_TOP = 3 + SPRITE_POINTER_BASE
.const BROOM_STRAIGHT_TOP = 5 + SPRITE_POINTER_BASE
.const PAVEL_BACK_STARE_TOP = 6 + SPRITE_POINTER_BASE
.const COMPUTER_TOP = 7 + SPRITE_POINTER_BASE

.const PAVEL_SWEEP_0_BOTTOM = 0 + 8  + SPRITE_POINTER_BASE
.const PAVEL_SWEEP_1_BOTTOM = 2 + 8  + SPRITE_POINTER_BASE
.const PAVEL_KNEEL_BOTTOM = 4 + 8 + SPRITE_POINTER_BASE
.const BROOM_SWEEP_0_BOTTOM = 1 + 8  + SPRITE_POINTER_BASE
.const BROOM_SWEEP_1_BOTTOM = 3 + 8  + SPRITE_POINTER_BASE
.const BROOM_STRAIGHT_BOTTOM = 5 + 8  + SPRITE_POINTER_BASE
.const PAVEL_BACK_STARE_BOTTOM = 6 + 8  + SPRITE_POINTER_BASE
.const COMPUTER_BOTTOM = 7 + 8  + SPRITE_POINTER_BASE

 .const PROFESSOR_LEFT = 16  + SPRITE_POINTER_BASE
 .const PROFESSOR_RIGHT = 17  + SPRITE_POINTER_BASE
 .const PAVEL_LOOK_RIGHT = 18  + SPRITE_POINTER_BASE

 .const MULTI_COLOR_1 = LIGHT_RED
 .const MULTI_COLOR_2 = GREY

 .const BROOM_COLOR = ORANGE
.const PAVEL_COLOR = BLUE
.const COMPUTER_COLOR = GREEN
.const PROFESSOR_COLOR = LIGHT_GREY

.const ANIMATION_FRAME_COUNT = 20

//----------------- TEXTS --------------
.const WRITE_EVENT = 0;
.const STANDARD_FONT_EVENT = 1| $80
.const FAKE_RUSSIAN_FONT_EVENT = 2| $80
.const SETUP_INTR_OPENING = 3| $80
.const LOOK_UP_EVENT = 4| $80
.const SETUP_INTRO_2 = 5|$80
.const SETUP_INTRO_3 = 6|$80
.const SETUP_GAME_SCREEN = 7|$80

//--------------------------------
.const scoreThreshold1 = 8
.const scoreThreshold2 = 60


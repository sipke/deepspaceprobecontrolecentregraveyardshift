.import source "constants.asm"

.macro clearScreenM(){
	jsr $e544
   	lax #0   
	stx $d020
	stx $d021
!:	lda #0
	sta $d800,x
	sta $d800+250,x
	sta $d800+500,x
	sta $d800+750,x
	inx
	cpx #250
	bne !-
}

.macro writeCharsRectM(){
	lda #0
	sta TMP
	lda #>PHOTO_SCREEN_MEM+128
	sta sm1+2
	lda #<PHOTO_SCREEN_MEM+128
	sta sm1+1
	lda #>$d800+128
	sta sm2+2
	lda #<$d800+128
	sta sm2+1
loop:
	ldy #$10
outer:	
	ldx #$0
inner:
	lda TMP
sm1:sta PHOTO_SCREEN_MEM+128,x
	clc
	adc #$10
	sta TMP
	lda #1
sm2:sta $d800+128,x
	inx
	cpx #$10
	bne inner
	lda sm1+1
	clc 
	adc #$28
	sta sm1+1
	sta sm2+1
	bcc !+
	inc sm1+2
	inc sm2+2
!:	inc TMP
	dey
	bne outer
}

//clears 8 blocks of mem
.macro clearDotsM()
{
	lda #>PHOTO_WORK_AREA
	sta sm1+2
	ldy #8
outer:	
	lax #0
inner:
sm1:
	sta PHOTO_WORK_AREA,x
	inx
	bne inner
	inc sm1 + 2
	dey
	bne outer
/*
	lax #0
!:	
	.for(var i=0;i<8;i++){
		sta PHOTO_WORK_AREA+i*256,x
	}
	inx
	bne !-
	*/
}

/* calculates address from X and Y memory 
	writes to $fc
	puts offset in y
*/
.macro calcAddressFromXYM()
{
	lda #0
	sta WRITE_ADDRESS_ZP
	lax X1	
	lsr 
	lsr 
	lsr 
	lsr 
	clc
	adc #>PHOTO_WORK_AREA
	sta WRITE_ADDRESS_ZP + 1
	txa
	and #%00001000
	bne column2
	lda yDot
	tay
	rts
column2:
	lda yDot
	ora #%10000000
doy:	
!:	tay
	rts
}


.macro drawlineM()
{
	lda X2
	and #$F8
	sta tempByte
	lda X1
	and #$F8
	cmp tempByte
	bne moreThanOneByte
	jmp doTheOneByte
moreThanOneByte:
	lda X2
	and #%11111000
	sta xEndLeftAligned	//aligned to first bit of byte containing xEnd, when currentXLeftAligned is same you are in last byte

	lax X1
	and #%11111000
	sta currentXLeftAligned

//write first starting byte	
	txa
	and #$07
	tax
	lda bitsstart,x
	and mask
	ora (WRITE_ADDRESS_ZP),y
	sta (WRITE_ADDRESS_ZP),y
loop:	
//next byte, write address = + 128	
	lda WRITE_ADDRESS_ZP
	clc 
	adc #$80
	sta WRITE_ADDRESS_ZP
	bcc !+
	inc WRITE_ADDRESS_ZP + 1
!:	lda currentXLeftAligned
	clc
	adc #8
	sta currentXLeftAligned
	cmp xEndLeftAligned
	bne notend
	lda X2
	and #$07
	tax
	lda bitsend,x
	and mask
	ora (WRITE_ADDRESS_ZP),y
	sta (WRITE_ADDRESS_ZP),y
	jmp end
notend:
	lda #$ff
	and mask
	ora (WRITE_ADDRESS_ZP),y
	sta (WRITE_ADDRESS_ZP),y
	jmp loop
end:
	rts
doTheOneByte:

	lda X1
	and #$07
	tax
	lda bitsstart,x
	sta tempByte

	lda X2
	and #$07
	tax
	lda bitsend,x
	and tempByte
	and mask

	sta tempByte
	ora (WRITE_ADDRESS_ZP),y
	sta (WRITE_ADDRESS_ZP),y
	rts	



bitsstart:	.fill 8,pow(2,(8-i)) - 1
bitsend:	.fill 8,255-pow(2,(7-i)) + 1
xEndLeftAligned:	.byte 0
currentXLeftAligned:	.byte 0
}	

.macro drawPlanetM(drawline, calcAddressFromXY) {
	clearDotsM()
	lda #0
	sta count
loop:	
	lda #%10101010
	sta mask

	ldx count
//-------------	
	lda START_POINTS,x

	bmi next
	sta X1
	//lda yStart,x	//values 0-127
	//sta Y
	stx yDot
	lda SWITCH_POINTS,x
	sta X2
	cmp X1
	beq lightPart
//add linelength to score	
	sec
	sbc X1
	clc
	adc score
	sta score
	lda score +1
	adc #0
	sta score +1

	jsr calcAddressFromXY
	jsr drawline

lightPart:
	lda #%11111111
	sta mask
	ldx count
//----------	
	lda SWITCH_POINTS,x
	sta X1
	//lda yStart,x	//values 0-127
	//sta Y
	stx yDot
	lda END_POINTS,x
	sta X2
	cmp X1
	beq next
//add linelength to score	
	sec
	sbc X1
	lsr	//light side half score is easier
	clc
	adc score
	sta score
	lda score +1
	adc #0
	sta score +1

	jsr calcAddressFromXY
	jsr drawline

next:
	ldx count
	inx
	cpx #$80
	beq done
	stx count
	jmp loop
done:
//drawoutline
	lda #%11111111
	sta mask
	lda #0
	sta yDot
	lda #0
	sta X1
	lda #127
	sta X2
	jsr calcAddressFromXY
	jsr drawline
loopy:	
	inc yDot
	bmi steppy
	lda #127
	sta X1	//X2 already 127
	jsr calcAddressFromXY
	jsr drawline

	lda #0
	sta X1
	sta X2
	jsr calcAddressFromXY
	jsr drawline
	jmp loopy
steppy:
	lda #127
	sta yDot
	sta X2 // X1 already 0
	jsr calcAddressFromXY
	jsr drawline
	rts
}
//.import source "macros.asm"


.pc = $0801 "Basic upstart"
:BasicUpstart($080d)		

.const WRITE_ADDRESS_ZP = $fc
.const X1 = $fe
.const X2 = $ff

*= $080d
setup:
{
	ldx #0
	stx $d020
	stx $d021
!:	lda #32
	sta $0400,x
	sta $0400+250,x
	sta $0400+500,x
	sta $0400+750,x
	lda #0
	sta $d800,x
	sta $d800+250,x
	sta $d800+500,x
	sta $d800+750,x
	inx
	cpx #250
	bne !-

//TODO: no speed code!
	// build char map

/*	
	.for(var y=0; y<16;y++){
		.for(var x=0; x<16;x++) {
			lda #(x*16)+y
			sta $0400+8 + x +(y+3)*40
			lda #1
			sta $d800+8 + x +(y+3)*40
		}
	}
	*/
.memblock "temp speedcode"
loop:
	ldy #$10
outer:	
	ldx #$0
inner:
	lda val
sm1:sta $0400+128,x
	clc
	adc #$10
	sta val
	lda #1
sm2:sta $d800+128,x
	inx
	cpx #$10
	bne inner
	lda sm1+1
	clc 
	adc #$28
	sta sm1+1
	sta sm2+1
	bcc !+
	inc sm1+2
	inc sm2+2
!:	inc val
	dey
	bne outer


.memblock "drawcode"
	//char set at $2000
	lda $d018
	and #%11110001
	ora #%00001001
	sta $d018

	jsr clear
}

	jsr clear
	jmp *
loop:	
	lda count
	and #$7f
	tax	
	lda xStart,x
	sta X1
	lda yStart,x
	sta Y
	lda xEnd,x
	sta X2
	jsr calcAddressFromXY
	jsr drawline
	
	inc count
// TODO just for now
	lda Y
	and #$01
	bne !+
	lda #%11111111
	sta mask
	jmp loop
!:	lda #%01010101
	sta mask
	jmp loop


/* calculates address from X and Y memory 
	writes to $fc
	puts offset in y
*/
calcAddressFromXY:
{
	lda #0
	sta WRITE_ADDRESS_ZP
	lax X1	
	lsr 
	lsr 
	lsr 
	lsr 
	clc
	adc #$20
	sta WRITE_ADDRESS_ZP + 1
	txa
	and #%00001000
	bne column2
	lda Y
	tay
	rts
column2:
	lda Y
	ora #%10000000
doy:	
!:	tay
	rts
}


drawline:
{
	lda X2
	and #%11111000
	sta xEndLeftAligned	//aligned to first bit of byte containing xEnd, when currentXLeftAligned is same you are in last byte

	lax X1
	and #%11111000
	sta currentXLeftAligned

//write first starting byte	
	txa
	and #$07
	tax
	lda bitsstart,x
	and mask
	sta (WRITE_ADDRESS_ZP),y
loop:	
//next byte, write address = + 128	
	lda WRITE_ADDRESS_ZP
	clc 
	adc #$80
	sta WRITE_ADDRESS_ZP
	bcc !+
	inc WRITE_ADDRESS_ZP + 1
!:	lda currentXLeftAligned
	clc
	adc #8
	sta currentXLeftAligned
	cmp xEndLeftAligned
	bne notend
	lda X2
	and #$07
	tax
	lda bitsend,x
	and mask
	sta (WRITE_ADDRESS_ZP),y
	jmp end
notend:
	lda #$ff
	and mask
	sta (WRITE_ADDRESS_ZP),y
	jmp loop
end:
	rts
}	
clear:
{
	lda #0
	ldx #0
!:	sta $2000,x
	sta $2100,x
	sta $2200,x
	sta $2300,x
	sta $2400,x
	sta $2500,x
	sta $2600,x
	sta $2700,x
	inx
	bne !-
	rts
}

/*
drawdot:
{
	lax x	//4
	lsr 
	lsr 
	lsr 
	lsr 
	clc
	adc #$20
	sta sm + 2
	sta sm2 + 2
	txa
	and #%00001000
	bne column2
	lda y
	jmp doy
column2:
	lda y
	ora #%10000000
doy:	
!:	tay
	txa
	and #$07
	tax
sm:
	lda $2000,y
	ora bits,x
sm2:	
	sta $2000,y
	rts
}
*/
val:	.byte 0
Y:		.byte 0
count: 	.byte 0
xEndLeftAligned:	.byte 0
currentXLeftAligned:	.byte 0
x:	.byte 0
y:	.byte 0
bits:	.fill 8,pow(2,(7-i))
bitsstart:	.fill 8,pow(2,(8-i)) - 1
bitsend:	.fill 8,255-pow(2,(7-i)) + 1
bitswitch:	.byte %01111111
			.byte %01111111	
			.byte %01011111	
			.byte %01011111	
			.byte %01010111	
			.byte %01010111	
			.byte %01010101	
			.byte %01010101	
mask:		.byte %01010101
mask1:		.byte %11111111

.memblock "temp tables"
xStart:	.fill 128, 32+sin(i*6.28/128)*32
yStart:	.fill 128,i
xEnd:	.fill 128, 64+sin(i*6.28/256)*32

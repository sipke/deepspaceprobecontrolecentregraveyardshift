//#importonce

.import source"constants.asm"
.import source"basic_functions.asm"

.macro generatePointers()
{
    
    ldy #$ff
sm:    
    lda textz
    bne notEnd
    iny 
    cpy #32
    beq end
    lda sm+1
    clc
    adc #1
    sta textPointersLo,y
    lda sm+2
    adc #0
    sta textPointersHi,y
notEnd:    
    inc sm +1
    bne !+
    inc sm + 2
!:  
    jmp sm
end:    
}
// subroutine, prints text index A at position X and Y
printTextAt:
{
    sta sm1 +1
    stx sm3 +1
    sty sm2 +1
        clc
sm2:    ldx #10
sm3:    ldy #10
        jsr SET_ROW_COL
sm1:    ldx #10
        lda textPointersLo,x
        ldy textPointersHi,x
        jsr PRINT_STRING
        rts

}
clearPreviousText:
{
    ldy textAtX
!:  ldx textAtY
    jsr SET_ROW_COL
//TODO: not working correctly, maybe not fix?    
    ldx previousTextLength
    lda #32
    jsr FUNC_CHROUT
    iny 
    dec previousTextLength
    bne !-
}
rts


textz:
.encoding "petscii_upper"
.byte 0
.byte COL_CODE_LIGHT_GREY
.text "PLEASE WAIT"
 .byte 0
.byte COL_CODE_LIGHT_GREY
.text "SPACE TO START"
 .byte 0
.byte COL_CODE_LIGHT_GREY
.text "1991, USSR"
 .byte 0
.byte COL_CODE_LIGHT_GREY
.text "DEEP SPACE PROBE CONTROL CENTRE"
 .byte 0
.byte COL_CODE_LIGHT_GREY
.text "PAVEL THE JANITOR"
 .byte 0
.byte COL_CODE_WHITE
.text "BONK!"
 .byte 0
.byte COL_CODE_CYAN
.text "PROFESSOR, WHAT HAPPENED?"
 .byte 0
.byte COL_CODE_GREY
.text "HEART ATTACK"
 .byte 0
.byte COL_CODE_CYAN
.text "I'LL CALL AMBULANCE"
 .byte 0
.byte COL_CODE_GREY
.text "NO TIME!"
 .byte 0
.byte COL_CODE_GREY
.text "PROGRAM IS IN CRUCIAL PHASE:"
 .byte 0
.byte COL_CODE_GREY
.text "PROBE PASSES PLANET X AT 100 KM/S"
 .byte 0
.byte COL_CODE_GREY
.text "CLOSEST POINT 1000 KM"
 .byte 0
.byte COL_CODE_GREY
.text "MUST POINT PROBE AT PLANET"
 .byte 0
.byte COL_CODE_GREY
.text "THEN TAKE PHOTO"
 .byte 0
.byte COL_CODE_GREY
.text "YOU CAN TAKE 6 PHOTOS"
 .byte 0
.byte COL_CODE_GREY
.text "YOU DO THIS IN MY PLACE"
 .byte 0
.byte COL_CODE_GREY
.text "FOR MOTHERLAND"
 .byte 0
.byte COL_CODE_GREY
.text "REMEMBER SIGNALS TRAVEL AT LIGHT SPEED"
 .byte 0
.byte COL_CODE_GREY
.text "299.792 KM/S"
 .byte 0
.byte COL_CODE_GREY
.text "YOU MUST TIME RIGHT!"
 .byte 0
.byte COL_CODE_CYAN
.text "PROFESSOR?"
 .byte 0
.byte COL_CODE_LIGHT_GREY
.text "2020"
 .byte 0
.byte COL_CODE_LIGHT_GREY
.text "PHOTOS DISCOVERED IN BASEMENT"
 .byte 0
.byte COL_CODE_LIGHT_GREY
.text "SCIENTISTS CONFUSED WHY ANYONE"
 .byte 0
.byte COL_CODE_LIGHT_GREY
.text "WOULD KEEP THESE BORING PHOTOS"
 .byte 0
.byte COL_CODE_LIGHT_GREY
.text "SCIENTISTS INTERESTED BY PHOTOS"
 .byte 0
.byte COL_CODE_LIGHT_GREY
.text "BUT MORE RESEARCH IS NEEDED"
 .byte 0
.byte COL_CODE_LIGHT_GREY
.text "PHOTOS OF NEW PLANET AMAZE SCIENTISTS"
 .byte 0
.byte COL_CODE_LIGHT_GREY
//     1234567890123456789012345678901234567890
.text "AGE 75 PAVEL BECOMES HONORARY PROFESSOR"
 .byte 0
.byte COL_CODE_LIGHT_GREY
.text "YOUR SCORE:"
 .byte 0
@lastText: 
.byte COL_CODE_LIGHT_GREY
.text "0"
 .byte 0

/*
times:
.byte 0 // "PLEASE WAIT"
.byte 1 //  "SPACE TO START"
.byte 2 // "USSR, 1991"
.byte 3 // "DEEP SPACE PROBE CONTROL CENTRE"
 .byte 4 //"PAVEL, THE JANITOR"
 .byte 5 // "BONK!"
 .byte 6 //"PROFESSOR, WHAT HAPPENED?"
 .byte 7 // "HEART ATTACK"
 .byte 8 //"I'LL CALL AMBULANCE"
 .byte 9 // "NO TIME!"
 .byte 10 // "PROGRAM IS IN CRUCIAL PHASE:"
 .byte 11 // "PROBE WILL PASS NEW PLANET AT 10 KM/S"
 .byte 12 // "CLOSEST POINT 1000 KM"
 .byte 13 // "YOU MUST POINT PROBE AT PLANET"
 .byte 14 // "THEN TAKE PHOTO"
 .byte 15 // "IT CAN TAKE 6 PHOTOS"
 .byte 16 // "YOU MUST DO THIS IN MY PLACE"
 .byte 17 // COL_CODE_GREY
.byte 18 // "FOR OUR MOTHERLAND"
 .byte 19 // "REMEMBER SIGNALS TRAVEL AT LIGHT SPEED"
 .byte 20 // "299.792 KM/S"
 .byte 21 // "YOU MUST TIME RIGHT"
 .byte 22 // "PROFESSOR?"
  .byte 23 // "2020"
  .byte 24 // "PHOTOS DISCOVERED IN BASEMENT"
 .byte 25 // "SCIENTISTS CONFUSED WHY ANYONE"
 .byte 26 // "WOULD KEEP THESE BORING PHOTOS"
 .byte 27 // "SCIENTISTS PUZZLED BY INTERESTING PHOTOS"
 .byte 28 // "THEY FIND IT INTERESTING"
 .byte 29 // "BUT MORE RESEARCH IS NEEDED"
 .byte 30 // "PHOTOS OF NEW PLANET BAFFLE SCIENTISTS"
 .byte 31 // "AGE 75 PAVEL GETS HONORARY DEGREE IN PHYSICS"
  .byte 0 // "Your score:"
 .byte 0


    .byte 0
*/
//------------------------------------
//event positive: 
//  write text event, 
//      byte 0: text index
//      byte 1 position YYYXXXXX
//      byte 2 duration 
//event negative:
//  next byte = event number

.var eventList = List()
.var eventParamList = List()
.var eventDurationList = List()
.macro writeEvent(index, Xpos, Ypos, duration) {
    .eval eventList.add(index)
    //.print "add event type" + index
    .eval eventParamList.add((Xpos <<3) | Ypos)
    //.print "add event param" + ((Xpos <<3) | Ypos)
    .eval eventDurationList.add(duration)
    //.print "add event duration" + duration
    .if(Ypos>7) {.error "Y position too high" + " for index" + index}
    .if(Xpos>31) {.error "X position too high" + " for index" + index}
}
.macro otherEvent(type, param, duration) {
    //.print "add event type" + type
    //.print "add event param" + param
    .eval eventList.add(type)
    .eval eventParamList.add(param)
    //.print "add event duration" + duration
    .eval eventDurationList.add(duration)
}
//.macro writeEvent(index, Xpos, Ypos, duration) {
.if(!SKIP_INTRO) {
    otherEvent(STANDARD_FONT_EVENT, 0, 10)
    writeEvent(2,9,2,20)// "USSR, 1991"
    writeEvent(3,3,2,20)// "DEEP SPACE PROBE CONTROL CENTRE"
    otherEvent(SETUP_INTR_OPENING,0,10) 
    writeEvent(4,9,2,50)//"PAVEL, THE JANITOR"
    writeEvent(5,27,6,2)// "BONK!"
    otherEvent(FAKE_RUSSIAN_FONT_EVENT,0,2)
    otherEvent(LOOK_UP_EVENT, 0,20)
    otherEvent(SETUP_INTRO_2, 0,10)
    writeEvent(6,0,5,15)//"PROFESSOR, WHAT HAPPENED?"
    writeEvent(7,20,7,15)// "HEART ATTACK"
    writeEvent(8,1,5,15)//"I'LL CALL AMBULANCE"
    writeEvent(9,22,7,15)// "NO TIME!"
    writeEvent(10,11,7,15)// "PROGRAM IN CRUCIAL PHASE:"
    writeEvent(11,7,7,25)// "PROBE WILL PASS NEW PLANET AT 10 KM/S"
    writeEvent(12,16,7,25)// "CLOSEST POINT 1000 KM"
    //           1         2         3
    //  123456789012345678901234567890123456789
    // "MUST POINT PROBE AT PLANET"
    writeEvent(13,12,7,25)
    // "THEN TAKE PHOTO"
    writeEvent(14,16,7,25)
    // "IT CAN TAKE 6 PHOTOS"
    writeEvent(15,15,7,25)
    // "YOU MUST DO THIS IN MY PLACE"
    writeEvent(16,14,7,25)
    // "FOR OUR MOTHERLAND"
    writeEvent(17,17,7,15)
    // "REMEMBER SIGNALS TRAVEL AT LIGHT SPEED"
    writeEvent(18,2,7,25)
    // "299.792 KM/S"
    writeEvent(19,19,7,25)
    // "YOU MUST TIME RIGHT"
    writeEvent(20,18,7,10)
    // "PROFESSOR?"
    writeEvent(21,5,5,60)
    otherEvent(SETUP_INTRO_3, 0,90)
}
    otherEvent(SETUP_GAME_SCREEN, 0,1)


events:
    .for(var i=0; i<eventList.size(); i++){
        .byte eventList.get(i)
        //.print "add event type" + eventList.get(i)
    }
eventParameters:
    .for(var i=0; i<eventList.size(); i++){
        .byte eventParamList.get(i)
        //.print "add event param" + eventParamList.get(i)
    }    
eventDurations:
    .for(var i=0; i<eventList.size(); i++){
        .byte eventDurationList.get(i)
        //.print "add event duration" + eventDurationList.get(i)
    }    

.eval MAX_EVENTS =  eventDurationList.size()


    